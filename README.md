# Example Recipes project

### Scope

Simple public REST API based on Symfony 4.x framework

* Doctrine ORM
* REST API
* Swagger
* Docker
* Prooph
* CQRS - without query segregation

#### Unit tests

- [x] CookBook context
- [ ] Crawler context
- [ ] SharedKernel

### How to run

Copy env file from dist

Install dependencies ```composer install```

Generate database ```php bin/console doctrine:migrations:migrate --no-interaction```