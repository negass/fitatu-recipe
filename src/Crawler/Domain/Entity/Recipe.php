<?php

declare(strict_types=1);

namespace Recipes\Crawler\Domain\Entity;

use DateTimeImmutable;
use DateTimeInterface;
use Recipes\SharedKernel\Domain\ValueObject\RecipeId;
use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Class Recipe
 */
class Recipe
{
    /** @var RecipeId */
    private $id;

    /** @var string */
    private $title;

    /** @var string */
    private $content;

    /** @var Url */
    private $address;

    /** @var Blog */
    private $blog;

    /** @var DateTimeInterface */
    private $createdDate;

    /**
     * @param string $title
     * @param string $content
     * @param Url $address
     */
    public function __construct(string $title, string $content, Url $address)
    {
        $this->id = RecipeId::generate();
        $this->title = $title;
        $this->address = $address;
        $this->content = $content;
        $this->createdDate = new DateTimeImmutable();
    }

    /**
     * @return RecipeId
     */
    public function getId(): RecipeId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return Url
     */
    public function getAddress(): Url
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return Blog
     */
    public function getBlog(): Blog
    {
        return $this->blog;
    }

    /**
     * @param Blog $blog
     */
    public function addBlog(Blog $blog)
    {
        $this->blog = $blog;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedDate(): DateTimeInterface
    {
        return $this->createdDate;
    }
}
