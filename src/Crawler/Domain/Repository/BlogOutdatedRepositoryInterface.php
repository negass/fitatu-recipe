<?php

declare(strict_types=1);

namespace Recipes\Crawler\Domain\Repository;

use DateTimeInterface;
use Recipes\Crawler\Domain\Entity\Blog;

/**
 * Interface BlogOutdatedRepositoryInterface
 */
interface BlogOutdatedRepositoryInterface
{
    /**
     * @param DateTimeInterface $interval
     *
     * @return Blog[]
     */
    public function getOutdated(DateTimeInterface $interval): array;
}
