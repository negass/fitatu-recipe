<?php

declare(strict_types=1);

namespace Recipes\Crawler\Domain\Repository\Exception;

use Exception;

/**
 * Class RecipeRepositoryException
 */
class RecipeRepositoryException extends Exception
{
}
