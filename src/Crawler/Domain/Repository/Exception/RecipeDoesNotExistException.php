<?php

declare(strict_types=1);

namespace Recipes\Crawler\Domain\Repository\Exception;

/**
 * Class RecipeDoesNotExistException
 */
class RecipeDoesNotExistException extends RecipeRepositoryException
{
}
