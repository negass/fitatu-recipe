<?php

declare(strict_types=1);

namespace Recipes\Crawler\Domain\Repository;

use Recipes\Crawler\Domain\Entity\Recipe;
use Recipes\Crawler\Domain\Repository\Exception\RecipeRepositoryException;
use Recipes\SharedKernel\Domain\ValueObject\RecipeId;

/**
 * Interface RecipeRepositoryInterface
 */
interface RecipeRepositoryInterface
{
    /**
     * @param RecipeId $recipeId
     *
     * @throws RecipeRepositoryException
     *
     * @return Recipe
     */
    public function get(RecipeId $recipeId);

    /**
     * @param Recipe $recipe
     */
    public function save(Recipe $recipe);
}
