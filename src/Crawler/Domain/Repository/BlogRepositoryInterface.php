<?php

declare(strict_types=1);

namespace Recipes\Crawler\Domain\Repository;

use Recipes\Crawler\Domain\Entity\Blog;
use Recipes\Crawler\Domain\Repository\Exception\BlogDoesNotExistException;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;

/**
 * Interface BlogRepositoryInterface
 */
interface BlogRepositoryInterface
{
    /**
     * @param BlogId $blogId
     *
     * @throws BlogDoesNotExistException
     *
     * @return Blog
     */
    public function get(BlogId $blogId);

    /**
     * @param Blog $blog
     *
     * @return void
     */
    public function save(Blog $blog);
}
