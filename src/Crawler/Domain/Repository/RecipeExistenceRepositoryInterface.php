<?php

declare(strict_types=1);

namespace Recipes\Crawler\Domain\Repository;

use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Interface RecipeExistenceRepositoryInterface
 */
interface RecipeExistenceRepositoryInterface
{
    /**
     * @param Url $recipeUrl
     *
     * @return bool
     */
    public function exist(Url $recipeUrl);
}
