<?php

declare(strict_types=1);

namespace Recipes\Crawler\Domain\Event;

use Recipes\SharedKernel\Domain\ValueObject\RecipeId;

/**
 * Class RecipeCreatedEvent
 */
class RecipeCreatedEvent
{

    /** @var RecipeId */
    private $recipeId;

    /**
     * @param RecipeId $recipeId
     */
    public function __construct(RecipeId $recipeId)
    {
        $this->recipeId = $recipeId;
    }

    /**
     * @return RecipeId
     */
    public function getRecipeId(): RecipeId
    {
        return $this->recipeId;
    }
}
