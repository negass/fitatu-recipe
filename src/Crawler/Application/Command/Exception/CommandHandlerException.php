<?php

declare(strict_types=1);

namespace Recipes\Crawler\Application\Command\Exception;

use Exception;

/**
 * Class CommandHandlerException
 */
class CommandHandlerException extends Exception
{
}
