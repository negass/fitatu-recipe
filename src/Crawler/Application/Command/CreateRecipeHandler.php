<?php

declare(strict_types=1);

namespace Recipes\Crawler\Application\Command;

use Exception;
use Prooph\Bundle\ServiceBus\EventBus;
use Recipes\Crawler\Application\Command\Exception\CommandHandlerException;
use Recipes\Crawler\Domain\Entity\Recipe;
use Recipes\Crawler\Domain\Event\RecipeCreatedEvent;
use Recipes\Crawler\Domain\Repository\BlogRepositoryInterface;

/**
 * Class CreateRecipeHandler
 */
class CreateRecipeHandler
{
    /** @var EventBus */
    private $eventBus;

    /** @var BlogRepositoryInterface */
    private $blogRepository;

    /**
     * @param EventBus $eventBus
     * @param BlogRepositoryInterface $blogRepository
     */
    public function __construct(EventBus $eventBus, BlogRepositoryInterface $blogRepository)
    {
        $this->eventBus = $eventBus;
        $this->blogRepository = $blogRepository;
    }

    /**
     * @param CreateRecipeCommand $command
     *
     * @throws CommandHandlerException
     */
    public function handle(CreateRecipeCommand $command)
    {
        try {
            $recipe = $this->createRecipe($command);
            $blog = $this->blogRepository->get($command->getBlogId());

            $blog->addRecipe($recipe);
            $this->blogRepository->save($blog);

            $this->eventBus->dispatch(new RecipeCreatedEvent($recipe->getId()));
        } catch (Exception $e) {
            throw new CommandHandlerException(
                "Error occurred during recipe saving process - {$e->getMessage()}",
                $e->getCode(),
                $e->getPrevious()
            );
        }
    }

    /**
     * @param CreateRecipeCommand $command
     *
     * @return Recipe
     */
    private function createRecipe(CreateRecipeCommand $command)
    {
        return new Recipe(
            $command->getTitle(),
            $command->getContent(),
            $command->getAddress()
        );
    }
}
