<?php

declare(strict_types=1);

namespace Recipes\Crawler\Application\Command;

use Recipes\SharedKernel\Domain\ValueObject\BlogId;
use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Class CreateRecipeCommand
 */
class CreateRecipeCommand
{
    /** @var BlogId */
    private $blogId;

    /** @var string */
    private $title;

    /** @var string */
    private $content;

    /** @var Url */
    private $address;

    /** @var Url */
    private $imageUrl;

    /**
     * @param BlogId $blogId
     * @param string $title
     * @param string $content
     * @param Url $address
     * @param Url $imageUrl
     */
    public function __construct(BlogId $blogId, string $title, string $content, Url $address, Url $imageUrl)
    {
        $this->blogId = $blogId;
        $this->title = $title;
        $this->content = $content;
        $this->address = $address;
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return BlogId
     */
    public function getBlogId(): BlogId
    {
        return $this->blogId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return Url
     */
    public function getAddress(): Url
    {
        return $this->address;
    }

    /**
     * @return Url
     */
    public function getImageUrl(): Url
    {
        return $this->imageUrl;
    }
}
