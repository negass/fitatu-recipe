<?php

declare(strict_types=1);

namespace Recipes\Crawler\Application\EventHandler;

use Recipes\CookBook\Domain\Event\BlogCreatedEvent;
use Recipes\Crawler\Application\Command\CreateRecipeCommand;
use Recipes\Crawler\Application\Command\CreateRecipeHandler;
use Recipes\Crawler\Domain\Entity\Blog;
use Recipes\Crawler\Domain\Repository\BlogRepositoryInterface;
use Recipes\Crawler\Domain\Repository\RecipeExistenceRepositoryInterface;
use Recipes\Crawler\Infrastructure\FeedReader\FeedInterface;
use Recipes\Crawler\Infrastructure\FeedReader\FeedReader;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;

/**
 * Class BlogCreatedHandler
 */
class BlogCreatedHandler
{
    const BLOG_ID = 'blog_id';

    /** @var BlogRepositoryInterface */
    private $blogRepo;

    /** @var FeedReader */
    private $feedReader;

    /** @var CreateRecipeHandler */
    private $createRecipeHandler;

    /** @var RecipeExistenceRepositoryInterface */
    private $recipeExistenceRepo;

    /**
     * @param BlogRepositoryInterface $blogRepo
     * @param FeedReader $feedReader
     * @param CreateRecipeHandler $createRecipeHandler
     * @param RecipeExistenceRepositoryInterface $recipeExistenceRepo
     */
    public function __construct(
        BlogRepositoryInterface $blogRepo,
        FeedReader $feedReader,
        CreateRecipeHandler $createRecipeHandler,
        RecipeExistenceRepositoryInterface $recipeExistenceRepo
    ) {
        $this->blogRepo = $blogRepo;
        $this->feedReader = $feedReader;
        $this->createRecipeHandler = $createRecipeHandler;
        $this->recipeExistenceRepo = $recipeExistenceRepo;
    }

    /**
     * @param BlogCreatedEvent $event
     */
    public function __invoke(BlogCreatedEvent $event)
    {
        $blog = $this->blogRepo->get($event->getBlogId());
        $channel = $this->feedReader->read($blog->getFeedAddress());

        $this->addRecipesToBlog($blog, $channel->getFeeds());
    }

    /**
     * @param Blog $blog
     * @param FeedInterface[] $feeds
     */
    private function addRecipesToBlog(Blog $blog, array $feeds = [])
    {
        foreach ($feeds as $feed) {
            // Todo move this to batch recipe creator
            if (!$this->recipeAlreadyExist($feed)) {
                $this->createRecipeHandler->handle(new CreateRecipeCommand(
                    $blog->getId(),
                    $feed->getTitle(),
                    $feed->getContent(),
                    $feed->getLink(),
                    $feed->getImageUrl()
                ));
            }
        }
    }

    /**
     * @param FeedInterface $feed
     *
     * @return bool
     */
    private function recipeAlreadyExist(FeedInterface $feed)
    {
        return $this->recipeExistenceRepo->exist($feed->getLink());
    }
}
