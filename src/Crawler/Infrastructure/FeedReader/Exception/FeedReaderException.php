<?php

declare(strict_types=1);

namespace Recipes\Crawler\Infrastructure\FeedReader\Exception;

use Exception;

/**
 * Class FeedReaderException
 */
class FeedReaderException extends Exception
{
}
