<?php

declare(strict_types=1);

namespace Recipes\Crawler\Infrastructure\FeedReader;

use DateTimeInterface;
use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Interface FeedInterface
 */
interface FeedInterface
{
    /**
     * @return string[]
     */
    public function getTags(): array;

    /**
     * @return Url
     */
    public function getLink(): Url;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @return Url
     */
    public function getImageUrl(): Url;

    /**
     * @return string
     */
    public function getContent(): string;

    /**
     * @return DateTimeInterface
     */
    public function getCreatedDate(): DateTimeInterface;

    /**
     * @return DateTimeInterface
     */
    public function getModificationDate(): DateTimeInterface;
}
