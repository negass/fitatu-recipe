<?php

declare(strict_types=1);

namespace Recipes\Crawler\Infrastructure\FeedReader\Adapter;

use DateTimeInterface;
use Recipes\Crawler\Infrastructure\FeedReader\FeedChannelInterface;
use Recipes\SharedKernel\Domain\ValueObject\FeedUrl;
use Zend\Feed\Reader\Feed\FeedInterface as ZendFeedInterface;

/**
 * Class ZendChannelAdapter
 */
class ZendChannelAdapter implements FeedChannelInterface
{

    /** @var ZendFeedInterface */
    private $zendFeed;

    /**
     * @param ZendFeedInterface $zendFeed
     */
    public function __construct(ZendFeedInterface $zendFeed)
    {
        $this->zendFeed = $zendFeed;
    }

    /**
     * {@inheritdoc}
     */
    public function getFeedUrl(): FeedUrl
    {
        return new FeedUrl($this->zendFeed->getFeedLink());
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedDate(): DateTimeInterface
    {
        return $this->zendFeed->getDateCreated();
    }

    /**
     * {@inheritdoc}
     */
    public function getModificationDate(): DateTimeInterface
    {
        return $this->zendFeed->getDateModified();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthor()
    {
        return $this->zendFeed->getAuthors();
    }

    /**
     * {@inheritdoc]
     */
    public function getFeeds(): array
    {
        $feeds = [];

        foreach ($this->zendFeed as $feed) {
            $feeds[] = new ZendFeedAdapter($feed);
        }

        return $feeds;
    }
}
