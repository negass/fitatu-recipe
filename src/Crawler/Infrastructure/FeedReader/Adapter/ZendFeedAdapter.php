<?php

declare(strict_types=1);

namespace Recipes\Crawler\Infrastructure\FeedReader\Adapter;

use DateTimeInterface;
use Recipes\Crawler\Infrastructure\FeedReader\FeedInterface;
use Recipes\SharedKernel\Domain\ValueObject\Url;
use Symfony\Component\DomCrawler\Crawler;
use Zend\Feed\Reader\Entry\EntryInterface;

/**
 * Class ZendFeedAdapter
 */
class ZendFeedAdapter implements FeedInterface
{
    const TAG_TERM_KEY = 'term';

    /** @var EntryInterface */
    private $zendFeed;

    /**
     * @param EntryInterface $zendFeed
     */
    public function __construct(EntryInterface $zendFeed)
    {
        $this->zendFeed = $zendFeed;
    }

    /**
     * {@inheritdoc}
     */
    public function getLink(): Url
    {
        return new Url($this->zendFeed->getLink());
    }

    /**
     * {@inheritdoc}
     */
    public function getTitle(): string
    {
        return $this->zendFeed->getTitle();
    }

    /**
     * {@inheritdoc}
     */
    public function getContent(): string
    {
        return strip_tags($this->zendFeed->getContent());
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedDate(): DateTimeInterface
    {
        return $this->zendFeed->getDateCreated();
    }

    /**
     * {@inheritdoc}
     */
    public function getModificationDate(): DateTimeInterface
    {
        return $this->zendFeed->getDateModified();
    }

    /**
     * {@inheritdoc}
     */
    public function getImageUrl(): Url
    {
        $content = new Crawler($this->zendFeed->getContent());

        return new Url(current($content->filter('img')->extract(['src'])));
    }

    /**
     * {@inheritdoc}
     */
    public function getTags(): array
    {
        $tags = [];

        foreach ($this->zendFeed->getCategories() as $category) {
            if (isset($category[self::TAG_TERM_KEY])) {
                $tags[] = $category[self::TAG_TERM_KEY];
            }
        }

        return $tags;
    }
}
