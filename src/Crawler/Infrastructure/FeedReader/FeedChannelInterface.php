<?php

declare(strict_types=1);

namespace Recipes\Crawler\Infrastructure\FeedReader;

use DateTimeInterface;
use Recipes\SharedKernel\Domain\ValueObject\FeedUrl;

/**
 * Interface FeedChannelInterface
 */
interface FeedChannelInterface
{
    /**
     * @return FeedInterface[]
     */
    public function getFeeds(): array;

    /**
     * @return FeedUrl
     */
    public function getFeedUrl(): FeedUrl;

    /**
     * @return DateTimeInterface
     */
    public function getCreatedDate(): DateTimeInterface;

    /**
     * @return DateTimeInterface
     */
    public function getModificationDate(): DateTimeInterface;
}
