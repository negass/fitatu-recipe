<?php

declare(strict_types=1);

namespace Recipes\Crawler\Infrastructure\FeedReader\Driver;

use GuzzleHttp\ClientInterface;
use Recipes\Crawler\Infrastructure\FeedReader\Adapter\ZendChannelAdapter;
use Recipes\SharedKernel\Domain\ValueObject\FeedUrl;
use Zend\Feed\Reader\Reader;

/**
 * Class ZendFeedReaderDriver
 */
class ZendFeedReaderDriver implements FeedReaderDriverInterface
{
    /** @var ClientInterface */
    private $httpClient;

    /**
     * @param ClientInterface $httpClient
     */
    public function __construct(ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }
    /**
     * {@inheritdoc}
     */
    public function read(FeedUrl $url)
    {
        return new ZendChannelAdapter(Reader::import($url->toString()));
    }
}
