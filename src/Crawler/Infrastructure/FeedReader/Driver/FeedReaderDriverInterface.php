<?php

declare(strict_types=1);

namespace Recipes\Crawler\Infrastructure\FeedReader\Driver;

use Recipes\Crawler\Infrastructure\FeedReader\Exception\FeedReaderException;
use Recipes\Crawler\Infrastructure\FeedReader\FeedChannelInterface;
use Recipes\Crawler\Infrastructure\FeedReader\FeedInterface;
use Recipes\SharedKernel\Domain\ValueObject\FeedUrl;

/**
 * Interface FeedReaderDriverInterface
 */
interface FeedReaderDriverInterface
{
    /**
     * @param FeedUrl $url
     *
     * @throws FeedReaderException
     *
     * @return FeedChannelInterface
     */
    public function read(FeedUrl $url);
}
