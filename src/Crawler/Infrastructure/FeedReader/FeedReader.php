<?php

declare(strict_types=1);

namespace Recipes\Crawler\Infrastructure\FeedReader;

use Recipes\Crawler\Infrastructure\FeedReader\Driver\FeedReaderDriverInterface;
use Recipes\Crawler\Infrastructure\FeedReader\Exception\FeedReaderException;
use Recipes\SharedKernel\Domain\ValueObject\FeedUrl;

/**
 * Class FeedReader
 */
class FeedReader
{
    /** @var FeedReaderDriverInterface */
    private $feedReaderDriver;

    /**
     * @param FeedReaderDriverInterface $feedReaderDriver
     */
    public function __construct(FeedReaderDriverInterface $feedReaderDriver)
    {
        $this->feedReaderDriver = $feedReaderDriver;
    }

    /**
     * @param FeedUrl $url
     *
     * @throws FeedReaderException
     *
     * @return FeedChannelInterface
     */
    public function read(FeedUrl $url)
    {
        return $this->feedReaderDriver->read($url);
    }
}
