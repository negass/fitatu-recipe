<?php

declare(strict_types=1);

namespace Recipes\Crawler\Infrastructure\Repository\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Recipes\Crawler\Domain\Entity\Blog;
use Recipes\Crawler\Domain\Repository\BlogRepositoryInterface;
use Recipes\Crawler\Domain\Repository\Exception\BlogDoesNotExistException;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;

/**
 * Class BlogRepository
 */
class BlogRepository implements BlogRepositoryInterface
{
    /** @var EntityRepository */
    private $repo;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repo = $entityManager->getRepository(Blog::class);
    }

    /**
     * {@inheritdoc}
     */
    public function get(BlogId $blogId)
    {
        $blog = $this->repo->createQueryBuilder('b')
            ->where('b.id = :blogId')
            ->setParameter('blogId', $blogId->toString())
            ->getQuery()
            ->getOneOrNullResult();

        if (!$blog) {
            throw new BlogDoesNotExistException("Blog with id {$blogId->toString()} does not exist");
        }

        return $blog;
    }

    /**
     * {@inheritdoc}
     */
    public function save(Blog $blog)
    {
        $this->entityManager->persist($blog);
        $this->entityManager->flush();

        return $blog;
    }
}
