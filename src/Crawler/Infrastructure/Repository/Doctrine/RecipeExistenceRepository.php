<?php

declare(strict_types=1);

namespace Recipes\Crawler\Infrastructure\Repository\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Recipes\Crawler\Domain\Entity\Recipe;
use Recipes\Crawler\Domain\Repository\RecipeExistenceRepositoryInterface;
use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Class RecipeExistenceRepository
 */
class RecipeExistenceRepository implements RecipeExistenceRepositoryInterface
{
    /** @var EntityRepository */
    private $repo;

    /** @var EntityManager */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repo = $entityManager->getRepository(Recipe::class);
    }

    /**
     * {@inheritdoc}
     */
    public function exist(Url $recipeUrl)
    {
        return (bool) $this->repo->createQueryBuilder('r')
            ->select('COUNT(1)')
            ->where('r.address.value = :address')
            ->setParameter('address', $recipeUrl->toString())
            ->getQuery()
            ->getSingleScalarResult();
    }
}
