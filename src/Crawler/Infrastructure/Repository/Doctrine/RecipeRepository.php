<?php

declare(strict_types=1);

namespace Recipes\Crawler\Infrastructure\Repository\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use Recipes\Crawler\Domain\Entity\Recipe;
use Recipes\Crawler\Domain\Repository\Exception\BlogDoesNotExistException;
use Recipes\Crawler\Domain\Repository\Exception\RecipeDoesNotExistException;
use Recipes\SharedKernel\Domain\ValueObject\RecipeId;
use Recipes\Crawler\Domain\Repository\RecipeRepositoryInterface;

/**
 * Class RecipeRepository
 */
class RecipeRepository implements RecipeRepositoryInterface
{
    /** @var EntityRepository */
    private $repo;

    /** @var EntityManager */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repo = $entityManager->getRepository(Recipe::class);
    }

    /**
     * {@inheritdoc}
     */
    public function get(RecipeId $recipeId)
    {
        $recipe = $this->repo->createQueryBuilder('r')
            ->where('r.id = :recipeId')
            ->setParameter('recipeId', $recipeId->toString())
            ->getQuery()
            ->getOneOrNullResult();

        if (!$recipe) {
            throw new RecipeDoesNotExistException("Recipe with id {$recipeId->toString()} does not exist");
        }

        return $recipe;
    }

    /**
     * {@inheritdoc}
     */
    public function save(Recipe $recipe)
    {
        try {
            $this->entityManager->persist($recipe);
            $this->entityManager->flush();

            return $recipe;
        } catch (ORMException $e) {
            throw new BlogDoesNotExistException($e, $e->getCode(), $e->getPrevious());
        }
    }
}
