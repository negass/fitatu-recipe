<?php

declare(strict_types=1);

namespace Recipes\Crawler\Infrastructure\Repository\Doctrine;

use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Recipes\Crawler\Domain\Entity\Blog;
use Recipes\Crawler\Domain\Repository\BlogOutdatedRepositoryInterface;

/**
 * Class BlogOutdatedRepository
 */
class BlogOutdatedRepository implements BlogOutdatedRepositoryInterface
{

    /** @var EntityRepository */
    private $repo;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repo = $entityManager->getRepository(Blog::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getOutdated(DateTimeInterface $interval): array
    {
        return $this->repo->createQueryBuilder('b')
            ->where('b.updated > :date')
            ->setParameter('date', $interval)
            ->getQuery()
            ->getResult();
    }

}
