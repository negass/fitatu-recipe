<?php

declare(strict_types=1);

namespace Recipes\SharedKernel\Infrastructure\Util\Doctrine\Traits;

use DateTimeInterface;

/**
 * Class Timestampable
 */
trait Timestampable
{
    /** @var DateTimeInterface */
    protected $created;

    /** @var DateTimeInterface */
    protected $updated;

    /**
     * @return DateTimeInterface
     */
    public function getCreated(): DateTimeInterface
    {
        return $this->created;
    }

    /**
     * @return DateTimeInterface
     */
    public function getUpdated(): DateTimeInterface
    {
        return $this->updated;
    }

    /**
     * @param DateTimeInterface $created
     */
    public function setCreated(DateTimeInterface $created)
    {
        $this->created = $created;
    }

    /**
     * @param DateTimeInterface $updated
     */
    public function setUpdated(DateTimeInterface $updated)
    {
        $this->updated = $updated;
    }
}
