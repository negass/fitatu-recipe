<?php

declare(strict_types=1);

namespace Recipes\SharedKernel\Infrastructure\Util\Common;

/**
 * Class ArrayUtil
 */
class ArrayUtil
{
    /**
     * @param array $data
     *
     * @return string[]
     */
    public static function toStringArray(array $data)
    {
        $result = [];

        foreach ($data as $singleData) {
            $result[] = (string)$singleData;
        }

        return $result;
    }
}
