<?php

declare(strict_types=1);

namespace Recipes\SharedKernel\Infrastructure\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Interface ActionInterface
 */
interface ActionInterface
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function run(Request $request);
}
