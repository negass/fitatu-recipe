<?php

declare(strict_types=1);

namespace Recipes\SharedKernel\Infrastructure\Rest\Exception;

use Throwable;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class RestException
 */
abstract class RestException extends HttpException
{
    const STATUS_CODE = 500;

    /**
     * @inheritdoc
     */
    public function __construct($message, Throwable $prev = null)
    {
        parent::__construct(static::STATUS_CODE, $message, $prev);
    }
}
