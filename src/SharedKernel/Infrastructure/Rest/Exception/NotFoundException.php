<?php

declare(strict_types=1);


namespace Recipes\SharedKernel\Infrastructure\Rest\Exception;

/**
 * Class NotFoundException
 */
class NotFoundException extends RestException
{
    const STATUS_CODE = 404;
}
