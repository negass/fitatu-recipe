<?php

declare(strict_types=1);

namespace Recipes\SharedKernel\Infrastructure\Rest\Exception;

/**
 * Class InvalidArgumentException
 */
class InvalidArgumentException extends RestException
{
    const STATUS_CODE = 422;
}
