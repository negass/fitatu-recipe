<?php

declare(strict_types=1);

namespace Recipes\SharedKernel\Infrastructure\Rest\Exception;

/**
 * Class BadRequestException
 */
class BadRequestException extends RestException
{
    const STATUS_CODE = 400;
}
