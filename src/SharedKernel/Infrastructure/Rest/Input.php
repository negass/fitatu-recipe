<?php

declare(strict_types=1);

namespace Recipes\SharedKernel\Infrastructure\Rest;

/**
 * Class Input
 */
class Input
{
    /** @var ParameterBag */
    public $all;

    /** @var ParameterBag */
    public $body;

    /** @var ParameterBag */
    public $query;

    /** @var ParameterBag */
    public $parameters;

    /**
     * @param array $query
     * @param array $body
     * @param array $parameters
     */
    public function __construct(array $query, array $body, array $parameters)
    {
        $this->body = new ParameterBag($body);
        $this->query = new ParameterBag($query);
        $this->parameters = new ParameterBag($parameters);
        $this->all = new ParameterBag($query + $body + $parameters);
    }
}
