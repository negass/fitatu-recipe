<?php

declare(strict_types=1);

namespace Recipes\SharedKernel\Infrastructure\Rest\Listener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class RestExceptionListener
 */
class RestExceptionListener
{
    const STATUS = 'status';
    const MESSAGE = 'message';

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if (!$exception instanceof HttpException) {
            $response = $this->genericResponse();
        } else {
            $response = $this->restResponse($exception);
        }

        $event->setResponse($response);
    }

    /**
     * @param HttpException $exception
     *
     * @return JsonResponse
     */
    private function restResponse(HttpException $exception)
    {
        return new JsonResponse([
            self::MESSAGE => $exception->getMessage()
        ], $exception->getStatusCode());
    }

    /**
     * @return JsonResponse
     */
    private function genericResponse()
    {
        return new JsonResponse([
            self::MESSAGE => "Internal server error occurred"
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
