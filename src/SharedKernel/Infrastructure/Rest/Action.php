<?php

declare(strict_types=1);

namespace Recipes\SharedKernel\Infrastructure\Rest;

use Recipes\SharedKernel\Infrastructure\Rest\Factory\InputFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class Action
 */
abstract class Action implements ActionInterface
{
    /** @var CheckInterface[] */
    protected $checks = [];

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function run(Request $request)
    {
        $input = InputFactory::createFromRequest($request);

        foreach ($this->checks as $check) {
            if ($check->appliesTo($input)) {
                $check->check($input);
            }
        }

        return new JsonResponse($this->execute($input));
    }

    /**
     * @param CheckInterface $check
     */
    public function registerCheck(CheckInterface $check)
    {
        $this->checks[] = $check;
    }

    /**
     * @param Input $input
     *
     * @return array
     */
    abstract protected function execute(Input $input): array;
}
