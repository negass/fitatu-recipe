<?php

declare(strict_types=1);

namespace Recipes\SharedKernel\Infrastructure\Rest;

use Recipes\SharedKernel\Infrastructure\Rest\Exception\RestException;

/**
 * Interface CheckInterface
 */
interface CheckInterface
{
    /**
     * @param Input $input
     *
     * @return bool
     */
    public function appliesTo(Input $input): bool;

    /**
     * @param Input $input
     *
     * @throws RestException
     */
    public function check(Input $input);
}
