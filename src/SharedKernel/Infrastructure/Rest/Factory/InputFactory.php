<?php

declare(strict_types=1);

namespace Recipes\SharedKernel\Infrastructure\Rest\Factory;

use Recipes\SharedKernel\Infrastructure\Rest\Input;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class InputFactory
 */
class InputFactory
{
    /**
     * @param Request $request
     *
     * @return Input
     */
    public static function createFromRequest(Request $request): Input
    {
        return new Input(
            $request->query->all(),
            $request->request->all(),
            $request->attributes->all()
        );
    }
}
