<?php

declare(strict_types=1);

namespace Recipes\SharedKernel\Test\Infrastructure\Rest;

use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;
use Recipes\SharedKernel\Infrastructure\Rest\Input;

/**
 * Class BaseActionTestCase
 */
abstract class BaseActionTestCase extends TestCase
{
    /** @var Input|MockInterface */
    protected $inputMock;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->inputMock = Mockery::mock(Input::class);
    }
}
