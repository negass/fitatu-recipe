<?php

declare(strict_types=1);

namespace Recipes\SharedKernel\Domain\ValueObject;

/**
 * Class FeedUrl
 */
final class FeedUrl
{
    /** @var string */
    private $address;

    /**
     * @param string $address
     */
    public function __construct(string $address)
    {
        //TODO add fancy validation

        $this->address = $address;
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->address;
    }
}
