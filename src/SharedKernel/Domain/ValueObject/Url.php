<?php

declare(strict_types=1);

namespace Recipes\SharedKernel\Domain\ValueObject;

/**
 * Class Url
 */
final class Url
{
    /** @var string */
    private $value;

    /**
     * @param string $address
     */
    public function __construct(string $address)
    {
        //TODO add fancy validation
        $this->value = $address;
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }
}
