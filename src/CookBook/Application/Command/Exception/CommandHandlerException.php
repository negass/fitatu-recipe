<?php

namespace Recipes\CookBook\Application\Command\Exception;

use Exception;

/**
 * Class CommandHandlerException
 *
 * @codeCoverageIgnore
 */
class CommandHandlerException extends Exception
{

}
