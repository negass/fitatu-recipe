<?php

declare(strict_types=1);

namespace Recipes\CookBook\Application\Command;

use Recipes\SharedKernel\Domain\ValueObject\FeedUrl;
use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Class CreateBlogCommand
 */
class CreateBlogCommand
{
    /** @var string */
    private $name;

    /** @var Url */
    private $address;

    /** @var FeedUrl */
    private $feedAddress;

    /**
     * @param string $name
     * @param Url $address
     * @param FeedUrl $feedAddress
     */
    public function __construct(string $name, Url $address, FeedUrl $feedAddress)
    {
        $this->name = $name;
        $this->address = $address;
        $this->feedAddress = $feedAddress;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Url
     */
    public function getAddress(): Url
    {
        return $this->address;
    }

    /**
     * @return FeedUrl
     */
    public function getFeedAddress(): FeedUrl
    {
        return $this->feedAddress;
    }
}
