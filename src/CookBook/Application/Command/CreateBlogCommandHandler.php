<?php

declare(strict_types=1);

namespace Recipes\CookBook\Application\Command;

use Prooph\Bundle\ServiceBus\EventBus;
use Recipes\CookBook\Application\Factory\BlogFactory;
use Recipes\CookBook\Domain\Entity\Blog;
use Recipes\CookBook\Domain\Event\BlogCreatedEvent;
use Recipes\CookBook\Domain\Repository\BlogRepositoryInterface;
use Recipes\CookBook\Domain\Repository\Exception\BlogRepositoryException;
use Recipes\CookBook\Application\Command\Exception\CommandHandlerException;

/**
 * Class CreateBlogCommandHandler
 */
class CreateBlogCommandHandler
{
    /** @var EventBus */
    private $eventBus;

    /** @var BlogFactory */
    private $blogFactory;

    /** @var BlogRepositoryInterface */
    private $blogRepository;

    /**
     * @param EventBus $eventBus
     * @param BlogFactory $blogFactory
     * @param BlogRepositoryInterface $blogRepository
     */
    public function __construct(EventBus $eventBus, BlogFactory $blogFactory, BlogRepositoryInterface $blogRepository)
    {
        $this->eventBus = $eventBus;
        $this->blogFactory = $blogFactory;
        $this->blogRepository = $blogRepository;
    }

    /**
     * @param CreateBlogCommand $command
     *
     * @throws CommandHandlerException
     */
    public function __invoke(CreateBlogCommand $command)
    {
        try {
            $blog = $this->blogFactory->createFromCommand($command);
            $this->persistBlog($blog);

            $this->eventBus->dispatch(new BlogCreatedEvent($blog->getId()));
        } catch (BlogRepositoryException $e) {
            throw new CommandHandlerException(
                'Error occurred during blog saving process',
                $e->getCode(),
                $e->getPrevious()
            );
        }
    }

    /**
     * @param Blog $blog
     *
     * @throws BlogRepositoryException
     */
    private function persistBlog(Blog $blog)
    {
        $this->blogRepository->save($blog);
    }
}
