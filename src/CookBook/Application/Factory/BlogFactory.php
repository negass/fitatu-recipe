<?php

declare(strict_types=1);

namespace Recipes\CookBook\Application\Factory;

use Recipes\CookBook\Application\Command\CreateBlogCommand;
use Recipes\CookBook\Domain\Entity\Blog;

/**
 * Class BlogFactory
 */
class BlogFactory
{
    /**
     * @param CreateBlogCommand $command
     *
     * @return Blog
     */
    public function createFromCommand(CreateBlogCommand $command): Blog
    {
        return new Blog(
            $command->getName(),
            $command->getAddress(),
            $command->getFeedAddress()
        );
    }
}
