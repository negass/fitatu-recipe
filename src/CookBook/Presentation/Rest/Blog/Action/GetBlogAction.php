<?php

declare(strict_types=1);

namespace Recipes\CookBook\Presentation\Rest\Blog\Action;

use Recipes\CookBook\Domain\Repository\BlogRepositoryInterface;
use Recipes\CookBook\Domain\Repository\Exception\BlogDoesNotExistException;
use Recipes\CookBook\Presentation\Rest\Blog\ResponseBuilder\BlogResponseBuilder;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;
use Recipes\SharedKernel\Infrastructure\Rest\Action;
use Recipes\SharedKernel\Infrastructure\Rest\Exception\NotFoundException;
use Recipes\SharedKernel\Infrastructure\Rest\Input;

/**
 * Class GetBlog
 */
class GetBlogAction extends Action
{
    const QUERY_BLOG_ID = 'blog_id';

    /** @var BlogRepositoryInterface */
    private $blogRepository;

    /** @var BlogResponseBuilder */
    private $blogResponseBuilder;

    /**
     * @param BlogRepositoryInterface $blogRepository
     * @param BlogResponseBuilder $blogResponseBuilder
     */
    public function __construct(BlogRepositoryInterface $blogRepository, BlogResponseBuilder $blogResponseBuilder)
    {
        $this->blogRepository = $blogRepository;
        $this->blogResponseBuilder = $blogResponseBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(Input $input): array
    {
        try {
            $blogId = $input->parameters->get(self::QUERY_BLOG_ID);
            $blog = $this->blogRepository->get(BlogId::fromString($blogId));

            return $this->blogResponseBuilder->build($blog);
        } catch (BlogDoesNotExistException $e) {
            throw new NotFoundException("Blog with given id does not exist");
        }
    }
}
