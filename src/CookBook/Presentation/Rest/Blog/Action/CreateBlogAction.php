<?php

declare(strict_types=1);

namespace Recipes\CookBook\Presentation\Rest\Blog\Action;

use Exception;
use Prooph\ServiceBus\CommandBus;
use Recipes\CookBook\Domain\Entity\Blog;
use Recipes\CookBook\Domain\Repository\BlogRepositoryInterface;
use Recipes\CookBook\Presentation\Rest\Blog\Factory\CreateBlogCommandFactory;
use Recipes\CookBook\Presentation\Rest\Blog\ResponseBuilder\BlogResponseBuilder;
use Recipes\SharedKernel\Domain\ValueObject\Url;
use Recipes\SharedKernel\Infrastructure\Rest\Action;
use Recipes\SharedKernel\Infrastructure\Rest\Exception\BadRequestException;
use Recipes\SharedKernel\Infrastructure\Rest\Input;

/**
 * Class CreateBlogAction
 */
class CreateBlogAction extends Action
{
    /** @var CommandBus */
    private $commandBus;

    /** @var BlogRepositoryInterface */
    private $blogRepository;

    /** @var BlogResponseBuilder */
    private $blogResponseBuilder;

    /** @var CreateBlogCommandFactory */
    private $commandFactory;

    /**
     * @param CommandBus $commandBus
     * @param BlogRepositoryInterface $blogRepository
     * @param BlogResponseBuilder $blogResponseBuilder
     * @param CreateBlogCommandFactory $commandFactory
     */
    public function __construct(
        CommandBus $commandBus,
        BlogRepositoryInterface $blogRepository,
        BlogResponseBuilder $blogResponseBuilder,
        CreateBlogCommandFactory $commandFactory
    ) {
        $this->commandBus = $commandBus;
        $this->blogRepository = $blogRepository;
        $this->commandFactory = $commandFactory;
        $this->blogResponseBuilder = $blogResponseBuilder;
    }


    /**
     * {@inheritdoc}
     */
    public function execute(Input $input): array
    {
        try {
            $command = $this->commandFactory->createFromInput($input);
            $this->commandBus->dispatch($command);

            return $this->blogResponseBuilder->build($this->getBlog($command->getAddress()));
        } catch (Exception $e) {
            throw new BadRequestException($e->getMessage(), $e);
        }
    }

    /**
     * @param Url $address
     *
     * @return Blog
     */
    private function getBlog(Url $address)
    {
        return $this->blogRepository->getByAddress($address);
    }
}
