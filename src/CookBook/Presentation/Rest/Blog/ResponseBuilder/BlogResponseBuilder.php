<?php

declare(strict_types=1);

namespace Recipes\CookBook\Presentation\Rest\Blog\ResponseBuilder;

use Recipes\CookBook\Domain\Entity\Blog;

/**
 * Class BlogResponseBuilder
 *
 * @codeCoverageIgnore
 */
class BlogResponseBuilder
{
    const ID = 'id';
    const NAME = 'name';
    const ADDRESS = 'address';
    const CREATED_AT = 'created_at';
    const FEED_ADDRESS = 'feed_address';

    /**
     * @param Blog $blog
     *
     * @return array
     */
    public function build(Blog $blog)
    {
        return [
            self::ID => $blog->getId()->toString(),
            self::NAME => $blog->getName(),
            self::ADDRESS => $blog->getAddress()->toString(),
            self::FEED_ADDRESS => $blog->getFeedAddress()->toString(),
            self::CREATED_AT => $blog->getCreatedDate(),
        ];
    }
}
