<?php

declare(strict_types=1);

namespace Recipes\CookBook\Presentation\Rest\Blog\Check;

use Ramsey\Uuid\Uuid;
use Recipes\SharedKernel\Infrastructure\Rest\CheckInterface;
use Recipes\SharedKernel\Infrastructure\Rest\Exception\InvalidArgumentException;
use Recipes\SharedKernel\Infrastructure\Rest\Input;

/**
 * Class BlogIdCheck
 */
class BlogIdCheck implements CheckInterface
{
    const BLOG_ID = 'blog_id';

    /**
     * {@inheritdoc}
     */
    public function appliesTo(Input $input): bool
    {
        return $input->parameters->has(self::BLOG_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function check(Input $input)
    {
        if (!Uuid::isValid($input->all->get(self::BLOG_ID))) {
            throw new InvalidArgumentException("Blog id is not in valid format");
        }
    }
}
