<?php

declare(strict_types=1);

namespace Recipes\CookBook\Presentation\Rest\Blog\Check;

use Recipes\CookBook\Domain\Repository\BlogExistenceRepositoryInterface;
use Recipes\CookBook\Presentation\Rest\Blog\Factory\BlogExistenceFilterFactory;
use Recipes\SharedKernel\Infrastructure\Rest\CheckInterface;
use Recipes\SharedKernel\Infrastructure\Rest\Exception\InvalidArgumentException;
use Recipes\SharedKernel\Infrastructure\Rest\Input;

/**
 * Class BlogExistenceCheck
 */
class BlogExistenceCheck implements CheckInterface
{
    const BLOG_ID = 'blog_id';

    /** @var BlogExistenceRepositoryInterface */
    private $blogExistenceRepo;

    /** @var BlogExistenceFilterFactory */
    private $blogExistenceFilterFactory;

    /**
     * @param BlogExistenceRepositoryInterface $blogExistenceRepo
     * @param BlogExistenceFilterFactory $blogExistenceFilterFactory
     */
    public function __construct(
        BlogExistenceRepositoryInterface $blogExistenceRepo,
        BlogExistenceFilterFactory $blogExistenceFilterFactory
    ) {
        $this->blogExistenceRepo = $blogExistenceRepo;
        $this->blogExistenceFilterFactory = $blogExistenceFilterFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function appliesTo(Input $input): bool
    {
        return $input->parameters->has(self::BLOG_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function check(Input $input)
    {
        $filter = $this->blogExistenceFilterFactory->createFromInput($input);

        if (!$this->blogExistenceRepo->exist($filter)) {
            throw new InvalidArgumentException("Blog with id {$filter->id()} does not exist");
        }
    }
}
