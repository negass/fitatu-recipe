<?php

declare(strict_types=1);

namespace Recipes\CookBook\Presentation\Rest\Blog\Factory;

use Recipes\CookBook\Application\Command\CreateBlogCommand;
use Recipes\SharedKernel\Domain\ValueObject\FeedUrl;
use Recipes\SharedKernel\Domain\ValueObject\Url;
use Recipes\SharedKernel\Infrastructure\Rest\Input;

/**
 * Class CreateBlogCommandFactory
 *
 * @codeCoverageIgnore
 */
class CreateBlogCommandFactory
{
    const KEY_NAME = 'name';
    const KEY_ADDRESS = 'address';
    const KEY_FEED_ADDRESS = 'feed_address';

    /**
     * @param Input $input
     *
     * @return CreateBlogCommand
     */
    public function createFromInput(Input $input)
    {
        return new CreateBlogCommand(
            $input->body->get(self::KEY_NAME),
            new Url($input->body->get(self::KEY_ADDRESS)),
            new FeedUrl($input->body->get(self::KEY_FEED_ADDRESS))
        );
    }
}
