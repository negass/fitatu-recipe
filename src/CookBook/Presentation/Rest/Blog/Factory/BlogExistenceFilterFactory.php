<?php

declare(strict_types=1);

namespace Recipes\CookBook\Presentation\Rest\Blog\Factory;

use Recipes\CookBook\Domain\DTO\BlogExistenceFilter;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;
use Recipes\SharedKernel\Domain\ValueObject\Url;
use Recipes\SharedKernel\Infrastructure\Rest\Input;

/**
 * Class BlogExistenceFilterFactory
 */
class BlogExistenceFilterFactory
{
    const BLOG_ID = 'blog_id';
    const BLOG_ADDRESS = 'address';

    /**
     * @param Input $input
     *
     * @return BlogExistenceFilter
     */
    public function createFromInput(Input $input)
    {
        $filter = new BlogExistenceFilter();

        if ($input->all->has(self::BLOG_ID)) {
            $filter->withId(BlogId::fromString($input->all->get(self::BLOG_ID)));
        }

        if ($input->all->has(self::BLOG_ADDRESS)) {
            $filter->withAddress(new Url($input->all->get(self::BLOG_ADDRESS)));
        }

        return $filter;
    }
}
