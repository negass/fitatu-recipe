<?php

declare(strict_types=1);

namespace Recipes\CookBook\Presentation\Rest\Recipe\ResponseBuilder;

use Recipes\CookBook\Domain\Entity\Recipe;

/**
 * Class RecipeResponseBuilder
 *
 * @codeCoverageIgnore
 */
class RecipeResponseBuilder
{
    const KEY_ID = 'id';
    const KEY_TITLE = 'title';
    const KEY_ADDRESS = 'address';
    const KEY_CONTENT = 'content';
    const KEY_CREATED = 'created_at';

    /**
     * @param Recipe $recipe
     *
     * @return array
     */
    public function build(Recipe $recipe): array
    {
        return [
            self::KEY_ID => $recipe->getId()->toString(),
            self::KEY_TITLE => $recipe->getTitle(),
            self::KEY_CONTENT => $recipe->getContent(),
            self::KEY_ADDRESS => $recipe->getAddress()->toString(),
            self::KEY_CREATED => $recipe->getCreatedDate(),
        ];
    }

    /**
     * @param Recipe[] $recipes
     *
     * @return array
     */
    public function buildForCollection(array $recipes): array
    {
        $result = [];

        foreach ($recipes as $recipe) {
            $result[] = $this->build($recipe);
        }

        return $result;
    }
}
