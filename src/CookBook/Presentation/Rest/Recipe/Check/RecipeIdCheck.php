<?php

declare(strict_types=1);

namespace Recipes\CookBook\Presentation\Rest\Recipe\Check;

use Ramsey\Uuid\Uuid;
use Recipes\SharedKernel\Infrastructure\Rest\CheckInterface;
use Recipes\SharedKernel\Infrastructure\Rest\Exception\InvalidArgumentException;
use Recipes\SharedKernel\Infrastructure\Rest\Input;

/**
 * Class BlogIdCheck
 */
class RecipeIdCheck implements CheckInterface
{
    const RECIPE_ID = 'recipe_id';

    /**
     * {@inheritdoc}
     */
    public function appliesTo(Input $input): bool
    {
        return $input->parameters->has(self::RECIPE_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function check(Input $input)
    {
        if (!Uuid::isValid($input->all->get(self::RECIPE_ID))) {
            throw new InvalidArgumentException("Recipe id is not in valid format");
        }
    }
}
