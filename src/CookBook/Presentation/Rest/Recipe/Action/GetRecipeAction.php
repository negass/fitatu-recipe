<?php

declare(strict_types=1);

namespace Recipes\CookBook\Presentation\Rest\Recipe\Action;

use Recipes\CookBook\Domain\Repository\Exception\RecipeDoesNotExistException;
use Recipes\CookBook\Domain\Repository\RecipeRepositoryInterface;
use Recipes\CookBook\Presentation\Rest\Recipe\ResponseBuilder\RecipeResponseBuilder;
use Recipes\SharedKernel\Domain\ValueObject\RecipeId;
use Recipes\SharedKernel\Infrastructure\Rest\Action;
use Recipes\SharedKernel\Infrastructure\Rest\Exception\NotFoundException;
use Recipes\SharedKernel\Infrastructure\Rest\Input;

/**
 * Class GetRecipeAction
 */
class GetRecipeAction extends Action
{
    const QUERY_RECIPE_ID = 'recipe_id';

    /** @var RecipeRepositoryInterface */
    private $recipeRepo;

    /** @var RecipeResponseBuilder */
    private $recipeResponseBuilder;

    /**
     * @param RecipeRepositoryInterface $recipeRepo
     * @param RecipeResponseBuilder $recipeResponseBuilder
     */
    public function __construct(RecipeRepositoryInterface $recipeRepo, RecipeResponseBuilder $recipeResponseBuilder)
    {
        $this->recipeRepo = $recipeRepo;
        $this->recipeResponseBuilder = $recipeResponseBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(Input $input): array
    {
        try {
            $recipeId = $input->parameters->get(self::QUERY_RECIPE_ID);
            $recipe = $this->recipeRepo->get(RecipeId::fromString($recipeId));

            return $this->recipeResponseBuilder->build($recipe);
        } catch (RecipeDoesNotExistException $e) {
            throw new NotFoundException("Recipe with given id does not exist");
        }
    }
}
