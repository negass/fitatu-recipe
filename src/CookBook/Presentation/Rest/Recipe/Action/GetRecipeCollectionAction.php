<?php

declare(strict_types=1);

namespace Recipes\CookBook\Presentation\Rest\Recipe\Action;

use Recipes\CookBook\Domain\DTO\RecipeCollectionFilter;
use Recipes\CookBook\Domain\Repository\RecipeRepositoryInterface;
use Recipes\CookBook\Presentation\Rest\Recipe\ResponseBuilder\RecipeResponseBuilder;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;
use Recipes\SharedKernel\Infrastructure\Rest\Action;
use Recipes\SharedKernel\Infrastructure\Rest\Input;

/**
 * Class GetRecipeCollectionAction
 */
class GetRecipeCollectionAction extends Action
{
    const QUERY_LIMIT = 'limit';
    const QUERY_OFFSET = 'offset';
    const QUERY_BLOG_ID = 'blog_id';

    /** @var RecipeRepositoryInterface */
    private $recipeRepo;

    /** @var RecipeResponseBuilder */
    private $recipeResponseBuilder;

    /**
     * @param RecipeRepositoryInterface $recipeRepo
     * @param RecipeResponseBuilder $recipeResponseBuilder
     */
    public function __construct(RecipeRepositoryInterface $recipeRepo, RecipeResponseBuilder $recipeResponseBuilder)
    {
        $this->recipeRepo = $recipeRepo;
        $this->recipeResponseBuilder = $recipeResponseBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(Input $input): array
    {
        $recipes = $this->recipeRepo->getCollection(new RecipeCollectionFilter(
            BlogId::fromString($input->parameters->get(self::QUERY_BLOG_ID)),
            $input->query->get(self::QUERY_LIMIT, 10),
            $input->query->get(self::QUERY_OFFSET, 0)
        ));

        return $this->recipeResponseBuilder->buildForCollection($recipes);
    }
}
