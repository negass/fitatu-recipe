<?php

declare(strict_types=1);

namespace Recipes\CookBook\Domain\Entity;

use Recipes\SharedKernel\Domain\ValueObject\BlogId;
use Recipes\SharedKernel\Domain\ValueObject\FeedUrl;
use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Class Blog
 */
class Blog
{

    /** @var BlogId */
    private $id;

    /** @var string */
    private $name;

    /** @var Url */
    private $address;

    /** @var FeedUrl */
    private $feedAddress;

    /** @var Recipe[] */
    private $recipes = [];

    /** @var \DateTimeInterface */
    private $createdDate;

    /**
     * @param string $name
     * @param Url $address
     * @param FeedUrl $feedAddress
     */
    public function __construct($name, Url $address, FeedUrl $feedAddress)
    {
        $this->id = BlogId::generate();
        $this->name = $name;
        $this->address = $address;
        $this->feedAddress = $feedAddress;
        $this->createdDate = new \DateTimeImmutable();
    }

    /**
     * @return BlogId
     */
    public function getId(): BlogId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Url
     */
    public function getAddress(): Url
    {
        return $this->address;
    }

    /**
     * @return FeedUrl
     */
    public function getFeedAddress(): FeedUrl
    {
        return $this->feedAddress;
    }

    /**
     * @return Recipe[]
     */
    public function getRecipes(): array
    {
        return $this->recipes;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param Recipe $recipe
     */
    public function addRecipe(Recipe $recipe)
    {
        $this->recipes[] = $recipe;
        $recipe->addBlog($this);
    }
}
