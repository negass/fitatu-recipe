<?php

declare(strict_types=1);

namespace Recipes\CookBook\Domain\Repository;

use Recipes\CookBook\Domain\Entity\Blog;
use Recipes\CookBook\Domain\Repository\Exception\BlogDoesNotExistException;
use Recipes\CookBook\Domain\Repository\Exception\BlogRepositoryException;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;
use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Interface BlogRepositoryInterface
 */
interface BlogRepositoryInterface
{
    /**
     * @param Blog $blog
     *
     * @throws BlogRepositoryException
     *
     * @return Blog
     */
    public function save(Blog $blog): Blog;

    /**
     * @param BlogId $blogId
     *
     * @throws BlogDoesNotExistException
     *
     * @return Blog
     */
    public function get(BlogId $blogId): Blog;

    /**
     * @param Url $address
     *
     * @throws BlogDoesNotExistException
     *
     * @return Blog
     */
    public function getByAddress(Url $address): Blog;
}
