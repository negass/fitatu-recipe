<?php

declare(strict_types=1);

namespace Recipes\CookBook\Domain\Repository;

use Recipes\CookBook\Domain\DTO\BlogExistenceFilter;

/**
 * Interface BlogExistenceRepositoryInterface
 */
interface BlogExistenceRepositoryInterface
{
    /**
     * @param BlogExistenceFilter $filter
     *
     * @return boolean
     */
    public function exist(BlogExistenceFilter $filter): bool;
}
