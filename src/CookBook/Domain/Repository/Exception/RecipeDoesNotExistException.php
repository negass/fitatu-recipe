<?php

declare(strict_types=1);

namespace Recipes\CookBook\Domain\Repository\Exception;

/**
 * Class RecipeDoesNotExistException
 *
 * @codeCoverageIgnore
 */
class RecipeDoesNotExistException extends RecipeRepositoryException
{
}
