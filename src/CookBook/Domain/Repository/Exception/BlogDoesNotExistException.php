<?php

declare(strict_types=1);

namespace Recipes\CookBook\Domain\Repository\Exception;

/**
 * Class BlogDoesNotExistException
 *
 * @codeCoverageIgnore
 */
class BlogDoesNotExistException extends BlogRepositoryException
{
}
