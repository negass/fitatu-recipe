<?php

declare(strict_types=1);

namespace Recipes\CookBook\Domain\Repository\Exception;

use Exception;

/**
 * Class RecipeRepositoryException
 *
 * @codeCoverageIgnore
 */
class RecipeRepositoryException extends Exception
{
}
