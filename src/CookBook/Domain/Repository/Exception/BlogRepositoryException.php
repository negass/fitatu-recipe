<?php

declare(strict_types=1);

namespace Recipes\CookBook\Domain\Repository\Exception;

use Exception;

/**
 * Class BlogRepositoryException
 *
 * @codeCoverageIgnore
 */
class BlogRepositoryException extends Exception
{
}
