<?php

declare(strict_types=1);

namespace Recipes\CookBook\Domain\Repository;

use Recipes\CookBook\Domain\DTO\RecipeCollectionFilter;
use Recipes\CookBook\Domain\Entity\Recipe;
use Recipes\CookBook\Domain\Repository\Exception\RecipeDoesNotExistException;
use Recipes\SharedKernel\Domain\ValueObject\RecipeId;

/**
 * Interface RecipeRepositoryInterface
 */
interface RecipeRepositoryInterface
{
    /**
     * @param RecipeId $recipeId
     *
     * @throws RecipeDoesNotExistException
     *
     * @return Recipe
     */
    public function get(RecipeId $recipeId): Recipe;

    /**
     * @param RecipeCollectionFilter $filter
     *
     * @return Recipe[]
     */
    public function getCollection(RecipeCollectionFilter $filter): array;
}
