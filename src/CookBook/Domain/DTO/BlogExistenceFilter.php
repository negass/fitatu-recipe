<?php

declare(strict_types=1);

namespace Recipes\CookBook\Domain\DTO;

use Recipes\SharedKernel\Domain\ValueObject\BlogId;
use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Class BlogExistenceFilter
 */
class BlogExistenceFilter
{
    /** @var BlogId|null */
    private $id;

    /** @var Url|null */
    private $address;

    /**
     * @param BlogId $blogId
     *
     * @return $this
     */
    public function withId(?BlogId $blogId)
    {
        $this->id = $blogId;

        return $this;
    }

    /**
     * @param Url $address
     *
     * @return $this
     */
    public function withAddress(?Url $address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return null|BlogId
     */
    public function id(): ?BlogId
    {
        return $this->id;
    }

    /**
     * @return null|Url
     */
    public function address(): ?Url
    {
        return $this->address;
    }

    /**
     * @return bool
     */
    public function hasAddress(): bool
    {
        return !is_null($this->address);
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }
}
