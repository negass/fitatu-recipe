<?php

declare(strict_types=1);

namespace Recipes\CookBook\Domain\DTO;

use Recipes\SharedKernel\Domain\ValueObject\BlogId;

/**
 * Class RecipeCollectionFilter
 */
class RecipeCollectionFilter
{
    /** @var BlogId */
    private $blogId;

    /** @var int */
    private $limit;

    /** @var int */
    private $offset;

    /**
     * @param BlogId $blogId
     * @param int $limit
     * @param int $offset
     */
    public function __construct(BlogId $blogId, int $limit, int $offset)
    {
        $this->setLimit($limit);
        $this->setBlogId($blogId);
        $this->setOffset($offset);
    }

    /**
     * @return BlogId
     */
    public function blogId(): BlogId
    {
        return $this->blogId;
    }

    /**
     * @return int
     */
    public function limit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function offset(): int
    {
        return $this->offset;
    }

    /**
     * @param BlogId $blogId
     */
    private function setBlogId(BlogId $blogId)
    {
        $this->blogId = $blogId;
    }

    /**
     * @param int $limit
     *
     * @throws \InvalidArgumentException
     */
    private function setLimit(int $limit)
    {
        if ($limit < 1) {
            throw new \InvalidArgumentException("Invalid limit value");
        }

        $this->limit = $limit;
    }

    /**
     * @param int $offset
     *
     * @throws \InvalidArgumentException
     */
    private function setOffset(int $offset)
    {
        if ($offset < 0) {
            throw new \InvalidArgumentException("Invalid offset value");
        }

        $this->offset = $offset;
    }
}
