<?php

declare(strict_types=1);

namespace Recipes\CookBook\Domain\Event;

use Recipes\SharedKernel\Domain\ValueObject\BlogId;

/**
 * Class BlogCreatedEvent
 */
class BlogCreatedEvent
{
    /** @var BlogId */
    private $blogId;

    /**
     * @param BlogId $blogId
     */
    public function __construct(BlogId $blogId)
    {
        $this->blogId = $blogId;
    }

    /**
     * @return BlogId
     */
    public function getBlogId(): BlogId
    {
        return $this->blogId;
    }
}
