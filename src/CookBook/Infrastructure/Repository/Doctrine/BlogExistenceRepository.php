<?php

declare(strict_types=1);

namespace Recipes\CookBook\Infrastructure\Repository\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Recipes\CookBook\Domain\DTO\BlogExistenceFilter;
use Recipes\CookBook\Domain\Entity\Blog;
use Recipes\CookBook\Domain\Repository\BlogExistenceRepositoryInterface;

/**
 * Class BlogExistenceRepository
 */
class BlogExistenceRepository implements BlogExistenceRepositoryInterface
{
    /** @var EntityRepository */
    private $repo;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repo = $entityManager->getRepository(Blog::class);
    }

    /**
     * {@inheritdoc}
     */
    public function exist(BlogExistenceFilter $filter): bool
    {
        $builder = $this->repo->createQueryBuilder('b')->select('b.id');

        if ($filter->hasId()) {
            $builder->andWhere('b.id = :id')->setParameter('id', $filter->id()->toString());
        }

        if ($filter->hasAddress()) {
            $builder->andWhere('b.address.value = :address')->setParameter('address', $filter->address()->toString());
        }

        return (bool)$builder->getQuery()->getOneOrNullResult();
    }
}
