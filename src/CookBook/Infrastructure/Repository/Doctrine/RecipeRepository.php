<?php

declare(strict_types=1);

namespace Recipes\CookBook\Infrastructure\Repository\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Recipes\CookBook\Domain\DTO\RecipeCollectionFilter;
use Recipes\CookBook\Domain\Entity\Recipe;
use Recipes\CookBook\Domain\Repository\Exception\RecipeDoesNotExistException;
use Recipes\CookBook\Domain\Repository\RecipeRepositoryInterface;
use Recipes\SharedKernel\Domain\ValueObject\RecipeId;

/**
 * Class RecipeRepository
 */
class RecipeRepository implements RecipeRepositoryInterface
{
    /** @var EntityRepository */
    private $repo;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repo = $entityManager->getRepository(Recipe::class);
    }

    /**
     * {@inheritdoc}
     */
    public function get(RecipeId $recipeId): Recipe
    {
        $recipe = $this->repo->createQueryBuilder('r')
            ->where('r.id = :recipeId')
            ->setParameter('recipeId', $recipeId->toString())
            ->getQuery()
            ->getOneOrNullResult();

        if (!$recipe) {
            throw new RecipeDoesNotExistException("Recipe with id {$recipeId->toString()} does not exist");
        }

        return $recipe;
    }


    /**
     * {@inheritdoc}
     */
    public function getCollection(RecipeCollectionFilter $filter): array
    {
        return $this->repo->createQueryBuilder('r')
            ->where('r.blog = :blogId')
            ->setParameter('blogId', $filter->blogId()->toString())
            ->setMaxResults($filter->limit())
            ->setFirstResult($filter->offset())
            ->getQuery()
            ->getResult();
    }
}
