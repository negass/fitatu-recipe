<?php

declare(strict_types=1);

namespace Recipes\CookBook\Infrastructure\Repository\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Recipes\CookBook\Domain\Entity\Blog;
use Recipes\CookBook\Domain\Repository\BlogRepositoryInterface;
use Recipes\CookBook\Domain\Repository\Exception\BlogDoesNotExistException;
use Recipes\CookBook\Domain\Repository\Exception\BlogRepositoryException;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;
use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Class BlogRepository
 */
class BlogRepository implements BlogRepositoryInterface
{
    /** @var EntityRepository */
    private $repo;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repo = $entityManager->getRepository(Blog::class);
    }

    /**
     * {@inheritdoc}
     */
    public function get(BlogId $blogId): Blog
    {
        $blog = $this->repo->createQueryBuilder('b')
            ->where('b.id = :blogId')
            ->setParameter('blogId', $blogId->toString())
            ->getQuery()
            ->getOneOrNullResult();

        if (!$blog) {
            throw new BlogDoesNotExistException("Blog with id {$blogId->toString()} does not exist");
        }

        return $blog;
    }

    /**
     * {@inheritdoc}
     */
    public function getByAddress(Url $blogAddress): Blog
    {
        $blog = $this->repo->createQueryBuilder('b')
            ->where('b.address.value = :address')
            ->setParameter('address', $blogAddress->toString())
            ->getQuery()
            ->getOneOrNullResult();

        if (!$blog) {
            throw new BlogDoesNotExistException("Blog with address {$blogAddress->toString()} does not exist");
        }

        return $blog;
    }

    /**
     * {@inheritdoc}
     */
    public function save(Blog $blog): Blog
    {
        try {
            $this->entityManager->persist($blog);
            $this->entityManager->flush();

            return $blog;
        } catch (\Exception $e) {
            throw new BlogRepositoryException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
    }
}
