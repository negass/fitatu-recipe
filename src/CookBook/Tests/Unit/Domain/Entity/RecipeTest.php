<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Domain\Entity;

use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;
use Recipes\CookBook\Domain\Entity\Blog;
use Recipes\CookBook\Domain\Entity\Recipe;
use Recipes\SharedKernel\Domain\ValueObject\RecipeId;
use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Class RecipeTest
 *
 * @covers \Recipes\CookBook\Domain\Entity\Recipe
 */
class RecipeTest extends TestCase
{
    const TITLE = 'title';
    const CONTENT = 'content';

    /** @var Recipe */
    private $sut;

    /** @var Blog|MockInterface */
    private $blogMock;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->blogMock = Mockery::mock(Blog::class);

        $this->sut = new Recipe(
            self::TITLE,
            self::CONTENT,
            self::getAddress()
        );
    }

    /** */
    public function testGetId_ShouldReturnCorrectValue()
    {
        $this->assertInstanceOf(RecipeId::class, $this->sut->getId());
    }

    /** */
    public function testGetTitle_ShouldReturnCorrectValue()
    {
        $this->assertEquals(self::TITLE, $this->sut->getTitle());
    }

    /** */
    public function testGetContent_ShouldReturnCorrectValue()
    {
        $this->assertEquals(self::CONTENT, $this->sut->getContent());
    }

    /** */
    public function testGetAddress_ShouldReturnCorrectValue()
    {
        $this->assertEquals($this->getAddress(), $this->sut->getAddress());
    }

    /** */
    public function testGetCreatedDate_ShouldReturnCorrectValue()
    {
        $this->assertInstanceOf(\DateTimeInterface::class, $this->sut->getCreatedDate());
    }

    /** */
    public function testGetBlog_ShouldReturnEmptyWhenBlogIsNotAttached()
    {
        $this->assertNull($this->sut->getBlog());
    }

    /** */
    public function testGetBlog_ShouldReturnBlogWhenIsAttached()
    {
        $this->sut->addBlog($this->blogMock);

        $this->assertInstanceOf(Blog::class, $this->sut->getBlog());
    }

    private function getAddress()
    {
        return new Url('http://recipe.url');
    }
}