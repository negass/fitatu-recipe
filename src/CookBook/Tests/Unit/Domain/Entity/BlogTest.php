<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Domain\Entity;

use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;
use Recipes\CookBook\Domain\Entity\Blog;
use Recipes\CookBook\Domain\Entity\Recipe;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;
use Recipes\SharedKernel\Domain\ValueObject\FeedUrl;
use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Class BlogTest
 *
 * @covers \Recipes\CookBook\Domain\Entity\Blog
 */
class BlogTest extends TestCase
{
    const NAME = 'name';
    const ADDRESS = 'http://address.url';
    const FEED_ADDRESS = 'http://feed-address.url';

    /** @var Blog */
    private $sut;

    /** @var Recipe|MockInterface */
    private $recipeMock;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->recipeMock = Mockery::mock(Recipe::class);

        $this->sut = new Blog(
            self::NAME,
            $this->getAddress(),
            $this->getFeedUrl()
        );
    }

    /** */
    public function testGetId_ShouldReturnCorrectValue()
    {
        $this->assertInstanceOf(BlogId::class, $this->sut->getId());
    }

    /** */
    public function testGetName_ShouldReturnCorrectValue()
    {
        $this->assertSame(self::NAME, $this->sut->getName());
    }

    /** */
    public function testGetAddress_ShouldReturnCorrectValue()
    {
        $this->assertEquals($this->getAddress(), $this->sut->getAddress());
    }

    /** */
    public function testGetFeedAddress_ShouldReturnCorrectValue()
    {
        $this->assertEquals($this->getFeedUrl(), $this->sut->getFeedAddress());
    }

    /** */
    public function testGetCreatedDate_ShouldReturnCorrectValue()
    {
        $this->assertInstanceOf(\DateTimeInterface::class, $this->sut->getCreatedDate());
    }

    /** */
    public function testGetRecipes_ShouldReturnEmptyArrayWhenThereIsNoRecipes()
    {
        $this->assertEquals([], $this->sut->getRecipes());
    }

    /** */
    public function testGetRecipes_ShouldReturnRecipesWhenGiven()
    {
        $this->recipeMock
            ->shouldReceive('addBlog')
            ->with(Mockery::type(Blog::class));

        $this->sut->addRecipe($this->recipeMock);

        $this->assertEquals([$this->recipeMock], $this->sut->getRecipes());
    }

    /**
     * @return Url
     */
    private function getAddress()
    {
        return new Url(self::ADDRESS);
    }

    /**
     * @return FeedUrl
     */
    private function getFeedUrl()
    {
        return new FeedUrl(self::FEED_ADDRESS);
    }
}