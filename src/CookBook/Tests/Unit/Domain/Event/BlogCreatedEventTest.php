<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Domain\Event;

use PHPUnit\Framework\TestCase;
use Recipes\CookBook\Domain\Event\BlogCreatedEvent;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;

/**
 * Class BlogCreatedEventTest
 *
 * @covers \Recipes\CookBook\Domain\Event\BlogCreatedEvent
 */
class BlogCreatedEventTest extends TestCase
{
    /** */
    public function testGetBlogId_ShouldReturnBlogId()
    {
        $blogId = BlogId::generate();

        $this->assertSame($blogId, (new BlogCreatedEvent($blogId))->getBlogId());
    }
}