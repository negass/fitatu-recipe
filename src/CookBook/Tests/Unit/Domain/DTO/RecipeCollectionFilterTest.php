<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Domain\DTO;

use PHPUnit\Framework\TestCase;
use Recipes\CookBook\Domain\DTO\RecipeCollectionFilter;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;

/**
 * Class RecipeCollectionFilterTest
 *
 * @covers \Recipes\CookBook\Domain\DTO\RecipeCollectionFilter
 */
class RecipeCollectionFilterTest extends TestCase
{
    const LIMIT = 20;
    const OFFSET = 10;
    const INVALID_LIMIT = 0;
    const INVALID_OFFSET = -1;
    const BLOG_ID = '01f3cf39-fb6e-11e6-874a-4c72b97ca1e7';

    /** @var RecipeCollectionFilter */
    private $sut;

    /** @var BlogId */
    private $blogId;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->blogId = BlogId::fromString(self::BLOG_ID);
        $this->sut = new RecipeCollectionFilter($this->blogId, self::LIMIT, self::OFFSET);
    }

    /** */
    public function testBlogId_ShouldReturnCorrectValue()
    {
        $this->assertEquals($this->blogId, $this->sut->blogId());
    }

    /** */
    public function testOffset_ShouldReturnCorrectValue()
    {
        $this->assertEquals(self::OFFSET, $this->sut->offset());
    }

    /** */
    public function test_ShouldThrowExceptionWhenOffsetIsNotValid()
    {
        $this->expectException(\InvalidArgumentException::class);

        new RecipeCollectionFilter($this->blogId, self::LIMIT, self::INVALID_OFFSET);
    }

    /** */
    public function testLimit_ShouldReturnCorrectValue()
    {
        $this->assertEquals(self::LIMIT, $this->sut->limit());
    }

    /** */
    public function test_ShouldThrowExceptionWhenLimitIsNotValid()
    {
        $this->expectException(\InvalidArgumentException::class);

        new RecipeCollectionFilter($this->blogId, self::INVALID_LIMIT, self::INVALID_OFFSET);
    }

    /** */
    public function test_ShouldThrowExceptionWhenLimitAndOffsetIsNotValid()
    {
        $this->expectException(\InvalidArgumentException::class);

        new RecipeCollectionFilter($this->blogId, self::INVALID_LIMIT, self::INVALID_OFFSET);
    }
}