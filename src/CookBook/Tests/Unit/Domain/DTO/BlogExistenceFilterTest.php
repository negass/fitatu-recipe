<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Domain\DTO;

use PHPUnit\Framework\TestCase;
use Recipes\CookBook\Domain\DTO\BlogExistenceFilter;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;
use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Class BlogExistenceFilterTest
 *
 * @covers \Recipes\CookBook\Domain\DTO\BlogExistenceFilter
 */
class BlogExistenceFilterTest extends TestCase
{
    const ADDRESS = 'http://blog.url';

    /** @var BlogExistenceFilter */
    private $sut;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->sut = new BlogExistenceFilter();
    }

    /** */
    public function testBlogId_ShouldReturnNullWhenIsNotGiven()
    {
        $this->assertNull($this->sut->id());
    }

    /** */
    public function testBlogId_ShouldReturnIdWhenIsGiven()
    {
        $blogId = BlogId::generate();
        $this->sut->withId($blogId);

        $this->assertSame($blogId, $this->sut->id());
    }

    /** */
    public function testAddress_ShouldReturnNullWhenIsNotGiven()
    {
        $this->assertNull($this->sut->address());
    }

    /** */
    public function testAddress_ShouldReturnAddressWhenIsGiven()
    {
        $address = new Url(self::ADDRESS);
        $this->sut->withAddress($address);

        $this->assertSame($address, $this->sut->address());
    }


    /** */
    public function testHasAddress_ShouldReturnTrueWhenIGiven()
    {
        $address = new Url(self::ADDRESS);
        $this->sut->withAddress($address);

        $this->assertTrue($this->sut->hasAddress());
    }

    /** */
    public function testHasAddress_ShouldReturnFalseWhenIsNotGiven()
    {
        $this->assertFalse($this->sut->hasAddress());
    }

    /** */
    public function testHasId_ShouldReturnTrueWhenIsGiven()
    {
        $blogId = BlogId::generate();
        $this->sut->withId($blogId);

        $this->assertTrue($this->sut->hasId());
    }

    /** */
    public function testHasId_ShouldReturnFalseWhenIsNotGiven()
    {
        $this->assertFalse($this->sut->hasId());
    }
}