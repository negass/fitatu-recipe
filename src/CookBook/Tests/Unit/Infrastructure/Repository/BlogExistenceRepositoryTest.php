<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Infrastructure\Repository;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\QueryBuilder;
use Mockery;
use Mockery\MockInterface;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Recipes\CookBook\Domain\DTO\BlogExistenceFilter;
use Recipes\CookBook\Domain\Entity\Blog;
use Recipes\CookBook\Infrastructure\Repository\Doctrine\BlogExistenceRepository;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;
use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Class BlogExistenceRepositoryTest
 *
 * @covers \Recipes\CookBook\Infrastructure\Repository\Doctrine\BlogExistenceRepository
 */
class BlogExistenceRepositoryTest extends TestCase
{
    const BLOG_ADDRESS = 'http://address.url';
    const BLOG_ID = '123e4567-e89b-12d3-a456-426655440000';

    /** @var BlogExistenceRepository */
    private $sut;

    /** @var AbstractQuery|MockInterface */
    private $queryMock;

    /** @var ObjectRepository|MockInterface */
    private $repositoryMock;

    /** @var QueryBuilder|MockInterface */
    private $queryBuilderMock;

    /** @var BlogExistenceFilter|MockInterface */
    private $filterMock;

    /** @var EntityManagerInterface|MockInterface */
    private $entityManagerMock;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->queryMock = Mockery::mock(AbstractQuery::class);
        $this->queryBuilderMock = Mockery::mock(QueryBuilder::class);
        $this->filterMock = Mockery::mock(BlogExistenceFilter::class);
        $this->repositoryMock = Mockery::mock(ObjectRepository::class);
        $this->entityManagerMock = Mockery::mock(EntityManagerInterface::class);

        $this->entityManagerMock
            ->shouldReceive('getRepository')
            ->with(Blog::class)
            ->andReturn($this->repositoryMock);

        $this->sut = new BlogExistenceRepository($this->entityManagerMock);
    }

    /** */
    public function testExist_ShouldReturnTrueForEmptyFilterWhenBlogExist()
    {
        $this->filterMock
            ->shouldReceive('hasId')
            ->andReturn(false);

        $this->filterMock
            ->shouldReceive('hasAddress')
            ->andReturn(false);

        $this->queryBuilderMock
            ->shouldReceive('select')
            ->with('b.id')
            ->andReturnSelf();

        $this->repositoryMock
            ->shouldReceive('createQueryBuilder')
            ->with('b')
            ->andReturn($this->queryBuilderMock);

        $this->queryMock
            ->shouldReceive('getOneOrNullResult')
            ->andReturn(123);

        $this->queryBuilderMock
            ->shouldReceive('getQuery')
            ->andReturn($this->queryMock);

        $this->assertTrue($this->sut->exist($this->filterMock));
    }

    /** */
    public function testExist_ShouldReturnFalseForEmptyFilterWhenBlogDoesExist()
    {
        $this->filterMock
            ->shouldReceive('hasId')
            ->andReturn(false);

        $this->filterMock
            ->shouldReceive('hasAddress')
            ->andReturn(false);

        $this->queryBuilderMock
            ->shouldReceive('select')
            ->with('b.id')
            ->andReturnSelf();

        $this->repositoryMock
            ->shouldReceive('createQueryBuilder')
            ->with('b')
            ->andReturn($this->queryBuilderMock);

        $this->queryMock
            ->shouldReceive('getOneOrNullResult')
            ->andReturn(null);

        $this->queryBuilderMock
            ->shouldReceive('getQuery')
            ->andReturn($this->queryMock);

        $this->assertFalse($this->sut->exist($this->filterMock));
    }


    /** */
    public function testExist_ShouldReturnTrueForFilterWithIdWhenBlogExist()
    {
        $this->filterMock
            ->shouldReceive('hasId')
            ->andReturn(true);

        $this->filterMock
            ->shouldReceive('id')
            ->andReturn(BlogId::fromString(self::BLOG_ID));

        $this->filterMock
            ->shouldReceive('hasAddress')
            ->andReturn(false);

        $this->queryBuilderMock
            ->shouldReceive('andWhere')
            ->with('b.id = :id')
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('setParameter')
            ->with('id', self::BLOG_ID)
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('select')
            ->with('b.id')
            ->andReturnSelf();

        $this->repositoryMock
            ->shouldReceive('createQueryBuilder')
            ->with('b')
            ->andReturn($this->queryBuilderMock);

        $this->queryMock
            ->shouldReceive('getOneOrNullResult')
            ->andReturn(123);

        $this->queryBuilderMock
            ->shouldReceive('getQuery')
            ->andReturn($this->queryMock);

        $this->assertTrue($this->sut->exist($this->filterMock));
    }

    /** */
    public function testExist_ShouldReturnTrueForFilterWithAddressWhenBlogExist()
    {
        $this->filterMock
            ->shouldReceive('hasId')
            ->andReturn(false);

        $this->filterMock
            ->shouldReceive('address')
            ->andReturn(new Url(self::BLOG_ADDRESS));

        $this->filterMock
            ->shouldReceive('hasAddress')
            ->andReturn(true);

        $this->queryBuilderMock
            ->shouldReceive('andWhere')
            ->with('b.address.value = :address')
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('setParameter')
            ->with('address', self::BLOG_ADDRESS)
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('select')
            ->with('b.id')
            ->andReturnSelf();

        $this->repositoryMock
            ->shouldReceive('createQueryBuilder')
            ->with('b')
            ->andReturn($this->queryBuilderMock);

        $this->queryMock
            ->shouldReceive('getOneOrNullResult')
            ->andReturn(123);

        $this->queryBuilderMock
            ->shouldReceive('getQuery')
            ->andReturn($this->queryMock);

        $this->assertTrue($this->sut->exist($this->filterMock));
    }

    /** */
    public function testExist_ShouldReturnTrueForFilterWithAddressAndIdWhenBlogExist()
    {
        $this->filterMock
            ->shouldReceive('hasId')
            ->andReturn(true);

        $this->filterMock
            ->shouldReceive('id')
            ->andReturn(BlogId::fromString(self::BLOG_ID));

        $this->filterMock
            ->shouldReceive('address')
            ->andReturn(new Url(self::BLOG_ADDRESS));

        $this->queryBuilderMock
            ->shouldReceive('andWhere')
            ->with('b.id = :id')
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('setParameter')
            ->with('id', self::BLOG_ID)
            ->andReturnSelf();

        $this->filterMock
            ->shouldReceive('hasAddress')
            ->andReturn(true);

        $this->queryBuilderMock
            ->shouldReceive('andWhere')
            ->with('b.address.value = :address')
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('setParameter')
            ->with('address', self::BLOG_ADDRESS)
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('select')
            ->with('b.id')
            ->andReturnSelf();

        $this->repositoryMock
            ->shouldReceive('createQueryBuilder')
            ->with('b')
            ->andReturn($this->queryBuilderMock);

        $this->queryMock
            ->shouldReceive('getOneOrNullResult')
            ->andReturn(123);

        $this->queryBuilderMock
            ->shouldReceive('getQuery')
            ->andReturn($this->queryMock);

        $this->assertTrue($this->sut->exist($this->filterMock));
    }
}