<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Infrastructure\Repository;

use Doctrine\ORM\ORMException;
use Mockery;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;
use Recipes\CookBook\Domain\Entity\Blog;
use Recipes\CookBook\Domain\Repository\Exception\BlogDoesNotExistException;
use Recipes\CookBook\Domain\Repository\Exception\BlogRepositoryException;
use Recipes\CookBook\Infrastructure\Repository\Doctrine\BlogRepository;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;
use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Class BlogRepositoryTest
 *
 * @covers \Recipes\CookBook\Infrastructure\Repository\Doctrine\BlogRepository
 */
class BlogRepositoryTest extends TestCase
{
    const BLOG_ADDRESS = 'http://blog.url';

    /** @var BlogRepository */
    private $sut;

    /** @var Blog|MockInterface */
    private $blogMock;

    /** @var AbstractQuery|MockInterface */
    private $queryMock;

    /** @var ObjectRepository|MockInterface */
    private $repositoryMock;

    /** @var QueryBuilder|MockInterface */
    private $queryBuilderMock;

    /** @var EntityManagerInterface|MockInterface */
    private $entityManagerMock;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->blogMock = Mockery::mock(Blog::class);
        $this->queryMock = Mockery::mock(AbstractQuery::class);
        $this->queryBuilderMock = Mockery::mock(QueryBuilder::class);
        $this->repositoryMock = Mockery::mock(ObjectRepository::class);
        $this->entityManagerMock = Mockery::mock(EntityManagerInterface::class);

        $this->entityManagerMock
            ->shouldReceive('getRepository')
            ->with(Blog::class)
            ->andReturn($this->repositoryMock);

        $this->sut = new BlogRepository($this->entityManagerMock);
    }

    /** */
    public function testGet_ShouldReturnBlogWhenExist()
    {
        $blogId = BlogId::generate();

        $this->repositoryMock
            ->shouldReceive('createQueryBuilder')
            ->with('b')
            ->andReturn($this->queryBuilderMock);

        $this->queryBuilderMock
            ->shouldReceive('where')
            ->with('b.id = :blogId')
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('setParameter')
            ->with('blogId', $blogId->toString())
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('getQuery')
            ->andReturn($this->queryMock);

        $this->queryMock
            ->shouldReceive('getOneOrNullResult')
            ->andReturn($this->blogMock);

        $this->assertSame($this->blogMock, $this->sut->get($blogId));
    }

    /** */
    public function testGet_ShouldThrowExceptionWhenBlogDoesNotExist()
    {
        $blogId = BlogId::generate();

        $this->repositoryMock
            ->shouldReceive('createQueryBuilder')
            ->with('b')
            ->andReturn($this->queryBuilderMock);

        $this->queryBuilderMock
            ->shouldReceive('where')
            ->with('b.id = :blogId')
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('setParameter')
            ->with('blogId', $blogId->toString())
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('getQuery')
            ->andReturn($this->queryMock);

        $this->queryMock
            ->shouldReceive('getOneOrNullResult')
            ->andReturn(null);

        $this->expectException(BlogDoesNotExistException::class);

        $this->sut->get($blogId);
    }

    /** */
    public function testGetByAddress_ShouldReturnBlogWhenExist()
    {
        $blogAddress = new Url(self::BLOG_ADDRESS);

        $this->repositoryMock
            ->shouldReceive('createQueryBuilder')
            ->with('b')
            ->andReturn($this->queryBuilderMock);

        $this->queryBuilderMock
            ->shouldReceive('where')
            ->with('b.address.value = :address')
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('setParameter')
            ->with('address', $blogAddress->toString())
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('getQuery')
            ->andReturn($this->queryMock);

        $this->queryMock
            ->shouldReceive('getOneOrNullResult')
            ->andReturn($this->blogMock);

        $this->assertSame($this->blogMock, $this->sut->getByAddress($blogAddress));
    }

    /** */
    public function testGetByAddress_ShouldThrowExceptionWhenBlogDoesNotExist()
    {
        $blogAddress = new Url(self::BLOG_ADDRESS);

        $this->repositoryMock
            ->shouldReceive('createQueryBuilder')
            ->with('b')
            ->andReturn($this->queryBuilderMock);

        $this->queryBuilderMock
            ->shouldReceive('where')
            ->with('b.address.value = :address')
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('setParameter')
            ->with('address', $blogAddress->toString())
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('getQuery')
            ->andReturn($this->queryMock);

        $this->queryMock
            ->shouldReceive('getOneOrNullResult')
            ->andReturn(null);

        $this->expectException(BlogDoesNotExistException::class);

        $this->sut->getByAddress($blogAddress);
    }

    /** */
    public function testSave_ShouldPersistBlogCorrectly()
    {
        $this->entityManagerMock
            ->shouldReceive('persist')
            ->with($this->blogMock);

        $this->entityManagerMock
            ->shouldReceive('flush')
            ->withNoArgs();

        $this->assertSame($this->blogMock, $this->sut->save($this->blogMock));
    }

    /** */
    public function testSave_ShouldThrowExceptionWhenPersistFailed()
    {
        $this->entityManagerMock
            ->shouldReceive('persist')
            ->with($this->blogMock);

        $this->entityManagerMock
            ->shouldReceive('flush')
            ->andThrow(ORMException::class);

        $this->expectException(BlogRepositoryException::class);

        $this->sut->save($this->blogMock);
    }
}