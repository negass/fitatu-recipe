<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Infrastructure\Repository;

use Mockery;
use Mockery\MockInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use PHPUnit\Framework\TestCase;
use Recipes\CookBook\Domain\DTO\RecipeCollectionFilter;
use Recipes\CookBook\Domain\Entity\Blog;
use Recipes\CookBook\Domain\Entity\Recipe;
use Recipes\CookBook\Domain\Repository\Exception\RecipeDoesNotExistException;
use Recipes\CookBook\Infrastructure\Repository\Doctrine\RecipeRepository;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;
use Recipes\SharedKernel\Domain\ValueObject\RecipeId;

/**
 * Class RecipeRepositoryTest
 *
 * @covers \Recipes\CookBook\Infrastructure\Repository\Doctrine\RecipeRepository
 */
class RecipeRepositoryTest extends TestCase
{
    const LIMIT = 10;
    const OFFSET = 5;

    /** @var RecipeRepository */
    private $sut;

    /** @var Recipe|MockInterface */
    private $recipeMock;

    /** @var AbstractQuery|MockInterface */
    private $queryMock;

    /** @var ObjectRepository|MockInterface */
    private $repositoryMock;

    /** @var QueryBuilder|MockInterface */
    private $queryBuilderMock;

    /** @var EntityManagerInterface|MockInterface */
    private $entityManagerMock;

    /** @var RecipeCollectionFilter|MockInterface */
    private $collectionFilterMock;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->recipeMock = Mockery::mock(Recipe::class);
        $this->queryMock = Mockery::mock(AbstractQuery::class);
        $this->queryBuilderMock = Mockery::mock(QueryBuilder::class);
        $this->repositoryMock = Mockery::mock(ObjectRepository::class);
        $this->entityManagerMock = Mockery::mock(EntityManagerInterface::class);
        $this->collectionFilterMock = Mockery::mock(RecipeCollectionFilter::class);

        $this->entityManagerMock
            ->shouldReceive('getRepository')
            ->with(Recipe::class)
            ->andReturn($this->repositoryMock);

        $this->sut = new RecipeRepository($this->entityManagerMock);
    }

    /** */
    public function testGet_ShouldReturnRecipeWhenExist()
    {
        $recipeId = RecipeId::generate();

        $this->repositoryMock
            ->shouldReceive('createQueryBuilder')
            ->with('r')
            ->andReturn($this->queryBuilderMock);

        $this->queryBuilderMock
            ->shouldReceive('where')
            ->with('r.id = :recipeId')
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('setParameter')
            ->with('recipeId', $recipeId->toString())
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('getQuery')
            ->andReturn($this->queryMock);

        $this->queryMock
            ->shouldReceive('getOneOrNullResult')
            ->andReturn($this->recipeMock);

        $this->assertSame($this->recipeMock, $this->sut->get($recipeId));
    }

    /** */
    public function testGet_ShouldThrowExceptionWhenRecipeDoesNotExist()
    {
        $recipeId = RecipeId::generate();

        $this->repositoryMock
            ->shouldReceive('createQueryBuilder')
            ->with('r')
            ->andReturn($this->queryBuilderMock);

        $this->queryBuilderMock
            ->shouldReceive('where')
            ->with('r.id = :recipeId')
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('setParameter')
            ->with('recipeId', $recipeId->toString())
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('getQuery')
            ->andReturn($this->queryMock);

        $this->queryMock
            ->shouldReceive('getOneOrNullResult')
            ->andReturn(null);

        $this->expectException(RecipeDoesNotExistException::class);

        $this->sut->get($recipeId);
    }

    /** */
    public function testGetCollection_ShouldReturnEmptyCollection()
    {
        $blogId = BlogId::generate();

        $this->collectionFilterMock
            ->shouldReceive('blogId')
            ->andReturn($blogId);

        $this->collectionFilterMock
            ->shouldReceive('offset')
            ->andReturn(self::OFFSET);

        $this->collectionFilterMock
            ->shouldReceive('limit')
            ->andReturn(self::LIMIT);

        $this->repositoryMock
            ->shouldReceive('createQueryBuilder')
            ->with('r')
            ->andReturn($this->queryBuilderMock);

        $this->queryBuilderMock
            ->shouldReceive('where')
            ->with('r.blog = :blogId')
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('setParameter')
            ->with('blogId', $blogId->toString())
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('setMaxResults')
            ->with(self::LIMIT)
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('setFirstResult')
            ->with(self::OFFSET)
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('getQuery')
            ->andReturn($this->queryMock);

        $this->queryMock
            ->shouldReceive('getResult')
            ->andReturn([]);

        $this->assertSame([], $this->sut->getCollection($this->collectionFilterMock));
    }

    /** */
    public function testGetCollection_ShouldReturnCollection()
    {
        $blogId = BlogId::generate();
        $blogMock = Mockery::mock(Blog::class);

        $this->collectionFilterMock
            ->shouldReceive('blogId')
            ->andReturn($blogId);

        $this->collectionFilterMock
            ->shouldReceive('offset')
            ->andReturn(self::OFFSET);

        $this->collectionFilterMock
            ->shouldReceive('limit')
            ->andReturn(self::LIMIT);

        $this->repositoryMock
            ->shouldReceive('createQueryBuilder')
            ->with('r')
            ->andReturn($this->queryBuilderMock);

        $this->queryBuilderMock
            ->shouldReceive('where')
            ->with('r.blog = :blogId')
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('setParameter')
            ->with('blogId', $blogId->toString())
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('setMaxResults')
            ->with(self::LIMIT)
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('setFirstResult')
            ->with(self::OFFSET)
            ->andReturnSelf();

        $this->queryBuilderMock
            ->shouldReceive('getQuery')
            ->andReturn($this->queryMock);

        $this->queryMock
            ->shouldReceive('getResult')
            ->andReturn([$blogMock]);

        $this->assertSame([$blogMock], $this->sut->getCollection($this->collectionFilterMock));
    }
}