<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Application\Factory;

use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;
use Recipes\CookBook\Application\Command\CreateBlogCommand;
use Recipes\CookBook\Application\Factory\BlogFactory;
use Recipes\CookBook\Domain\Entity\Blog;
use Recipes\SharedKernel\Domain\ValueObject\FeedUrl;
use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Class BlogFactoryTest
 *
 * @covers \Recipes\CookBook\Application\Factory\BlogFactory
 */
class BlogFactoryTest extends TestCase
{
    const BLOG_NAME = 'name';
    const BLOG_ADDRESS = 'http://address.url';
    const BLOG_FEED_ADDRESS = 'http://feed-address.url';

    /** @var BlogFactory */
    private $sut;

    /** @var CreateBlogCommand|MockInterface */
    private $commandMock;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->sut = new BlogFactory();

        $this->commandMock = Mockery::mock(CreateBlogCommand::class);
    }

    /** */
    public function testCreateFromCommand_ShouldReturnBlog()
    {
        $this->commandMock
            ->shouldReceive('getName')
            ->andReturn(self::BLOG_NAME);

        $this->commandMock
            ->shouldReceive('getAddress')
            ->andReturn(new Url(self::BLOG_ADDRESS));

        $this->commandMock
            ->shouldReceive('getFeedAddress')
            ->andReturn(new FeedUrl(self::BLOG_FEED_ADDRESS));

        $this->assertInstanceOf(Blog::class, $this->sut->createFromCommand($this->commandMock));
    }
}