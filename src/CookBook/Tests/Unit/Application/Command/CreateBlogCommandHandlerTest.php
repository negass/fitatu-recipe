<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Application\Command;

use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;
use Prooph\Bundle\ServiceBus\EventBus;
use Recipes\CookBook\Application\Command\CreateBlogCommand;
use Recipes\CookBook\Application\Command\Exception\CommandHandlerException;
use Recipes\CookBook\Application\Factory\BlogFactory;
use Recipes\CookBook\Domain\Entity\Blog;
use Recipes\CookBook\Domain\Event\BlogCreatedEvent;
use Recipes\CookBook\Domain\Repository\BlogRepositoryInterface;
use Recipes\CookBook\Application\Command\CreateBlogCommandHandler;
use Recipes\CookBook\Domain\Repository\Exception\BlogRepositoryException;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;

/**
 * Class CreateBlogCommandHandlerTest
 *
 * @covers \Recipes\CookBook\Application\Command\CreateBlogCommandHandler
 */
class CreateBlogCommandHandlerTest extends TestCase
{
    const BLOG_NAME = 'name';
    const BLOG_ADDRESS = 'http://blog-address.url';
    const BLOG_ID = '123e4567-e89b-12d3-a456-426655440000';
    const BLOG_FEED_ADDRESS = 'http://blog-feed-address.url';

    /** @var CreateBlogCommandHandler */
    private $sut;

    /** @var Blog|MockInterface */
    private $blogMock;

    /** @var EventBus|MockInterface */
    private $eventBusMock;

    /** @var BlogFactory|MockInterface */
    private $blogFactoryMock;

    /** @var BlogRepositoryInterface|MockInterface */
    private $blogRepositoryMock;

    /** @var CreateBlogCommand|MockInterface */
    private $createBlogCommandMock;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->blogMock = Mockery::mock(Blog::class);
        $this->eventBusMock = Mockery::mock(EventBus::class);
        $this->blogFactoryMock = Mockery::mock(BlogFactory::class);
        $this->createBlogCommandMock = Mockery::mock(CreateBlogCommand::class);
        $this->blogRepositoryMock = Mockery::mock(BlogRepositoryInterface::class);

        $this->sut = new CreateBlogCommandHandler(
            $this->eventBusMock,
            $this->blogFactoryMock,
            $this->blogRepositoryMock
        );
    }

    /** */
    public function testInvoke_ShouldCreateBlog()
    {
        $this->blogMock
            ->shouldReceive('getId')
            ->andReturn(BlogId::fromString(self::BLOG_ID));

        $this->blogFactoryMock
            ->shouldReceive('createFromCommand')
            ->with($this->createBlogCommandMock)
            ->andReturn($this->blogMock);

        $this->createBlogCommandMock
            ->shouldReceive('getName')
            ->andReturn(self::BLOG_NAME);

        $this->createBlogCommandMock
            ->shouldReceive('getAddress')
            ->andReturn(self::BLOG_ADDRESS);

        $this->createBlogCommandMock
            ->shouldReceive('getFeedAddress')
            ->andReturn(self::BLOG_FEED_ADDRESS);

        $this->eventBusMock
            ->shouldReceive('dispatch')
            ->with(Mockery::type(BlogCreatedEvent::class));

        $this->blogRepositoryMock
            ->shouldReceive('save')
            ->with($this->blogMock);

        $this->sut->__invoke($this->createBlogCommandMock);
    }

    /** */
    public function testInvoke_ShouldThrowCommandExceptionWhenSaveFailed()
    {
        $this->blogFactoryMock
            ->shouldReceive('createFromCommand')
            ->with($this->createBlogCommandMock)
            ->andReturn($this->blogMock);

        $this->createBlogCommandMock
            ->shouldReceive('getName')
            ->andReturn(self::BLOG_NAME);

        $this->createBlogCommandMock
            ->shouldReceive('getAddress')
            ->andReturn(self::BLOG_ADDRESS);

        $this->createBlogCommandMock
            ->shouldReceive('getFeedAddress')
            ->andReturn(self::BLOG_FEED_ADDRESS);

        $this->blogRepositoryMock
            ->shouldReceive('save')
            ->andThrow(BlogRepositoryException::class);

        $this->expectException(CommandHandlerException::class);

        $this->sut->__invoke($this->createBlogCommandMock);
    }
}