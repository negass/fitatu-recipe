<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Application\Command;

use PHPUnit\Framework\TestCase;
use Recipes\CookBook\Application\Command\CreateBlogCommand;
use Recipes\SharedKernel\Domain\ValueObject\FeedUrl;
use Recipes\SharedKernel\Domain\ValueObject\Url;

/**
 * Class CreateBlogCommandTest
 *
 * @covers \Recipes\CookBook\Application\Command\CreateBlogCommand;
 */
class CreateBlogCommandTest extends TestCase
{
    const NAME = 'example-name';
    const URL = 'http://example.url';
    const FEED_URL = 'http://example-feed.url';

    /** */
    public function testGetName_ShouldReturnExpectedName()
    {
        $address = new Url(self::URL);
        $feedUrl = new FeedUrl(self::FEED_URL);

        $command = new CreateBlogCommand(
            self::NAME,
            $address,
            $feedUrl
        );

        $this->assertSame(self::NAME, $command->getName());
        $this->assertSame($address, $command->getAddress());
        $this->assertSame($feedUrl, $command->getFeedAddress());
    }
}