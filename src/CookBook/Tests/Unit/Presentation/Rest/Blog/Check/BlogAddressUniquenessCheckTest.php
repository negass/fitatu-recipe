<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Presentation\Rest\Blog\Check;

use PHPUnit\Framework\TestCase;
use Mockery;
use Mockery\MockInterface;
use Recipes\CookBook\Domain\DTO\BlogExistenceFilter;
use Recipes\CookBook\Domain\Repository\BlogExistenceRepositoryInterface;
use Recipes\CookBook\Presentation\Rest\Blog\Check\BlogAddressUniquenessCheck;
use Recipes\CookBook\Presentation\Rest\Blog\Factory\BlogExistenceFilterFactory;
use Recipes\SharedKernel\Domain\ValueObject\Url;
use Recipes\SharedKernel\Infrastructure\Rest\Exception\InvalidArgumentException;
use Recipes\SharedKernel\Infrastructure\Rest\Input;
use Recipes\SharedKernel\Infrastructure\Rest\ParameterBag;

/**
 * Class BlogAddressUniquenessCheckTest
 *
 * @covers \Recipes\CookBook\Presentation\Rest\Blog\Check\BlogAddressUniquenessCheck
 */
class BlogAddressUniquenessCheckTest extends TestCase
{
    const ADDRESS_KEY = 'address';
    const ADDRESS_VALUE = 'http://random-address.url';

    /** @var BlogAddressUniquenessCheck */
    private $sut;

    /** @var Input|MockInterface */
    private $inputMock;

    /** @var BlogExistenceFilter|MockInterface */
    private $filterMock;

    /** @var BlogExistenceFilterFactory|MockInterface */
    private $blogExistenceFilterFactoryMock;

    /** @var BlogExistenceRepositoryInterface|MockInterface */
    private $blogExistenceRepositoryMock;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->inputMock = Mockery::mock(Input::class);
        $this->filterMock = Mockery::mock(BlogExistenceFilter::class);
        $this->blogExistenceFilterFactoryMock = Mockery::mock(BlogExistenceFilterFactory::class);
        $this->blogExistenceRepositoryMock = Mockery::mock(BlogExistenceRepositoryInterface::class);

        $this->sut = new BlogAddressUniquenessCheck(
            $this->blogExistenceRepositoryMock,
            $this->blogExistenceFilterFactoryMock
        );
    }

    /** */
    public function testAppliesTo_ShouldReturnTrue()
    {
        $this->inputMock->body = new ParameterBag([
            self::ADDRESS_KEY => self::ADDRESS_VALUE
        ]);

        $this->assertTrue($this->sut->appliesTo($this->inputMock));
    }

    /** */
    public function testAppliesTo_ShouldReturnFalse()
    {
        $this->inputMock->body = new ParameterBag();

        $this->assertFalse($this->sut->appliesTo($this->inputMock));
    }

    /** */
    public function testCheck_ShouldPassRequest()
    {
        $this->blogExistenceFilterFactoryMock
            ->shouldReceive('createFromInput')
            ->with($this->inputMock)
            ->andReturn($this->filterMock);

        $this->blogExistenceRepositoryMock
            ->shouldReceive('exist')
            ->with($this->filterMock)
            ->andReturn(false);

        $this->sut->check($this->inputMock);
    }

    /** */
    public function testCheck_ShouldThrowExceptionWhenBlogDoesNotExist()
    {
        $this->blogExistenceFilterFactoryMock
            ->shouldReceive('createFromInput')
            ->with($this->inputMock)
            ->andReturn($this->filterMock);

        $this->blogExistenceRepositoryMock
            ->shouldReceive('exist')
            ->with($this->filterMock)
            ->andReturn(true);

        $this->filterMock
            ->shouldReceive('address')
            ->andReturn(new Url(self::ADDRESS_VALUE));

        $this->expectException(InvalidArgumentException::class);

        $this->sut->check($this->inputMock);
    }
}
