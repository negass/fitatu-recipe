<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Presentation\Rest\Blog\Check;

use PHPUnit\Framework\TestCase;
use Mockery;
use Mockery\MockInterface;
use Recipes\CookBook\Domain\DTO\BlogExistenceFilter;
use Recipes\CookBook\Domain\Repository\BlogExistenceRepositoryInterface;
use Recipes\CookBook\Presentation\Rest\Blog\Check\BlogExistenceCheck;
use Recipes\CookBook\Presentation\Rest\Blog\Factory\BlogExistenceFilterFactory;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;
use Recipes\SharedKernel\Infrastructure\Rest\Exception\InvalidArgumentException;
use Recipes\SharedKernel\Infrastructure\Rest\Input;
use Recipes\SharedKernel\Infrastructure\Rest\ParameterBag;

/**
 * Class BlogExistenceCheckTest
 *
 * @covers \Recipes\CookBook\Presentation\Rest\Blog\Check\BlogExistenceCheck
 */
class BlogExistenceCheckTest extends TestCase
{
    const BLOG_ID_KEY = 'blog_id';
    const BLOG_ID_VALUE = '123e4567-e89b-12d3-a456-426655440000';

    /** @var BlogExistenceCheck */
    private $sut;

    /** @var Input|MockInterface */
    private $inputMock;

    /** @var BlogExistenceFilter|MockInterface */
    private $filterMock;

    /** @var BlogExistenceFilterFactory|MockInterface */
    private $blogExistenceFilterFactoryMock;

    /** @var BlogExistenceRepositoryInterface|MockInterface */
    private $blogExistenceRepositoryMock;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->inputMock = Mockery::mock(Input::class);
        $this->filterMock = Mockery::mock(BlogExistenceFilter::class);
        $this->blogExistenceFilterFactoryMock = Mockery::mock(BlogExistenceFilterFactory::class);
        $this->blogExistenceRepositoryMock = Mockery::mock(BlogExistenceRepositoryInterface::class);

        $this->sut = new BlogExistenceCheck(
            $this->blogExistenceRepositoryMock,
            $this->blogExistenceFilterFactoryMock
        );
    }

    /** */
    public function testAppliesTo_ShouldReturnTrue()
    {
        $this->inputMock->parameters = new ParameterBag([
            self::BLOG_ID_KEY => self::BLOG_ID_VALUE
        ]);

        $this->assertTrue($this->sut->appliesTo($this->inputMock));
    }

    /** */
    public function testAppliesTo_ShouldReturnFalse()
    {
        $this->inputMock->parameters = new ParameterBag();

        $this->assertFalse($this->sut->appliesTo($this->inputMock));
    }

    /** */
    public function testCheck_ShouldPassRequest()
    {
        $this->blogExistenceFilterFactoryMock
            ->shouldReceive('createFromInput')
            ->with($this->inputMock)
            ->andReturn($this->filterMock);

        $this->blogExistenceRepositoryMock
            ->shouldReceive('exist')
            ->with($this->filterMock)
            ->andReturn(true);

        $this->sut->check($this->inputMock);
    }

    /** */
    public function testCheck_ShouldThrowExceptionWhenBlogDoesNotExist()
    {
        $this->blogExistenceFilterFactoryMock
            ->shouldReceive('createFromInput')
            ->with($this->inputMock)
            ->andReturn($this->filterMock);

        $this->blogExistenceRepositoryMock
            ->shouldReceive('exist')
            ->with($this->filterMock)
            ->andReturn(false);

        $this->filterMock
            ->shouldReceive('id')
            ->andReturn(BlogId::fromString(self::BLOG_ID_VALUE));

        $this->expectException(InvalidArgumentException::class);

        $this->sut->check($this->inputMock);
    }
}
