<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Presentation\Rest\Blog\Factory;

use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;
use Recipes\CookBook\Domain\DTO\BlogExistenceFilter;
use Recipes\CookBook\Presentation\Rest\Blog\Factory\BlogExistenceFilterFactory;
use Recipes\SharedKernel\Infrastructure\Rest\Input;
use Recipes\SharedKernel\Infrastructure\Rest\ParameterBag;

/**
 * Class BlogExistenceFilterFactoryTest
 *
 * @covers \Recipes\CookBook\Presentation\Rest\Blog\Factory\BlogExistenceFilterFactory
 */
class BlogExistenceFilterFactoryTest extends TestCase
{
    const BLOG_ID_KEY = 'blog_id';
    const BLOG_ADDRESS_KEY = 'address';

    const BLOG_ID = '123e4567-e89b-12d3-a456-426655440000';
    const BLOG_ADDRESS = 'http://address.url';

    /** @var BlogExistenceFilterFactory */
    private $sut;

    /** @var Input|MockInterface */
    private $inputMock;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->inputMock = Mockery::mock(Input::class);

        $this->sut = new BlogExistenceFilterFactory();
    }

    /** */
    public function testCreateFromInput_ShouldReturnEmptyFilter()
    {
        $this->inputMock->all = new ParameterBag();

        $result = $this->sut->createFromInput($this->inputMock);

        $this->assertNull($result->id());
        $this->assertFalse($result->hasId());
        $this->assertNull($result->address());
        $this->assertFalse($result->hasAddress());
        $this->assertInstanceOf(BlogExistenceFilter::class, $result);
    }

    /** */
    public function testCreateFromInput_ShouldReturnFilterWithData()
    {
        $this->inputMock->all = new ParameterBag([
            self::BLOG_ID_KEY => self::BLOG_ID,
            self::BLOG_ADDRESS_KEY => self::BLOG_ADDRESS
        ]);

        $result = $this->sut->createFromInput($this->inputMock);

        $this->assertTrue($result->hasId());
        $this->assertTrue($result->hasAddress());
        $this->assertEquals(self::BLOG_ID, $result->id());
        $this->assertEquals(self::BLOG_ADDRESS, $result->address());
        $this->assertInstanceOf(BlogExistenceFilter::class, $result);
    }
}
