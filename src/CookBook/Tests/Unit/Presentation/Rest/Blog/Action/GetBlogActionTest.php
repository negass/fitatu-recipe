<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Presentation\Rest\Blog\Action;

use Mockery;
use Mockery\MockInterface;
use Recipes\CookBook\Domain\Entity\Blog;
use Recipes\CookBook\Domain\Repository\BlogRepositoryInterface;
use Recipes\CookBook\Domain\Repository\Exception\BlogDoesNotExistException;
use Recipes\CookBook\Presentation\Rest\Blog\Action\CreateBlogAction;
use Recipes\CookBook\Presentation\Rest\Blog\Action\GetBlogAction;
use Recipes\CookBook\Presentation\Rest\Blog\ResponseBuilder\BlogResponseBuilder;
use Recipes\SharedKernel\Domain\ValueObject\BlogId;
use Recipes\SharedKernel\Infrastructure\Rest\Exception\NotFoundException;
use Recipes\SharedKernel\Infrastructure\Rest\ParameterBag;
use Recipes\SharedKernel\Test\Infrastructure\Rest\BaseActionTestCase;

/**
 * Class GetBlogActionTest
 *
 * @covers \Recipes\CookBook\Presentation\Rest\Blog\Action\CreateBlogAction
 */
class GetBlogActionTest extends BaseActionTestCase
{
    const BLOG_ID = '123e4567-e89b-12d3-a456-426655440000';

    /** @var CreateBlogAction */
    private $sut;

    /** @var Blog|MockInterface */
    private $blogMock;

    /** @var BlogRepositoryInterface|MockInterface */
    private $blogRepositoryMock;

    /** @var BlogResponseBuilder|MockInterface */
    private $blogResponseBuilder;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();

        $this->blogMock = Mockery::mock(Blog::class);
        $this->blogRepositoryMock = Mockery::mock(BlogRepositoryInterface::class);
        $this->blogResponseBuilder = Mockery::mock(BlogResponseBuilder::class);

        $this->sut = new GetBlogAction(
            $this->blogRepositoryMock,
            $this->blogResponseBuilder
        );
    }

    /**
     * @param array $expectedResponse
     *
     * @dataProvider responseDataProvider
     */
    public function testRun_ShouldReturnBlog(array $expectedResponse)
    {
        $this->inputMock->parameters = new ParameterBag([
            'blog_id' => self::BLOG_ID
        ]);

        $this->blogRepositoryMock
            ->shouldReceive('get')
            ->with(Mockery::type(BlogId::class))
            ->andReturn($this->blogMock);

        $this->blogResponseBuilder
            ->shouldReceive('build')
            ->with($this->blogMock)
            ->andReturn($expectedResponse);

        $this->assertSame($expectedResponse, $this->sut->execute($this->inputMock));
    }

    /** */
    public function testRun_ShouldReturnThrowExceptionWhenBlogDoesNotExist()
    {
        $this->inputMock->parameters = new ParameterBag([
            'blog_id' => self::BLOG_ID
        ]);

        $this->blogRepositoryMock
            ->shouldReceive('get')
            ->andThrow(BlogDoesNotExistException::class);

        $this->expectException(NotFoundException::class);

        $this->sut->execute($this->inputMock);
    }

    /**
     * @return array
     */
    public function responseDataProvider()
    {
        return [
            [
                [
                    'id' => 123,
                    'name' => 'name',
                    'address' => 'http://address.url',
                    'feed_address' => 'http://feed-address.url',
                    'created_date' => '2008-11-11 13:23:44',
                ]
            ]
        ];
    }
}
