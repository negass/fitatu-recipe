<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Presentation\Rest\Blog\Action;

use Mockery;
use Mockery\MockInterface;
use Prooph\ServiceBus\CommandBus;
use Recipes\CookBook\Application\Command\CreateBlogCommand;
use Recipes\CookBook\Domain\Entity\Blog;
use Recipes\CookBook\Domain\Repository\BlogRepositoryInterface;
use Recipes\CookBook\Domain\Repository\Exception\BlogDoesNotExistException;
use Recipes\CookBook\Presentation\Rest\Blog\Action\CreateBlogAction;
use Recipes\CookBook\Presentation\Rest\Blog\Factory\CreateBlogCommandFactory;
use Recipes\CookBook\Presentation\Rest\Blog\ResponseBuilder\BlogResponseBuilder;
use Recipes\SharedKernel\Domain\ValueObject\Url;
use Recipes\SharedKernel\Infrastructure\Rest\Exception\BadRequestException;
use Recipes\SharedKernel\Test\Infrastructure\Rest\BaseActionTestCase;

/**
 * Class CreateBlogActionTest
 *
 * @covers \Recipes\CookBook\Presentation\Rest\Blog\Action\CreateBlogAction
 */
class CreateBlogActionTest extends BaseActionTestCase
{
    const BLOG_ADDRESS = 'http://address.url';

    /** @var CreateBlogAction */
    private $sut;

    /** @var Blog|MockInterface */
    private $blogMock;

    /** @var CommandBus|MockInterface */
    private $commandBusMock;

    /** @var CreateBlogCommand|MockInterface */
    private $createCommandMock;

    /** @var BlogRepositoryInterface|MockInterface */
    private $blogRepositoryMock;

    /** @var BlogResponseBuilder|MockInterface */
    private $blogResponseBuilder;

    /** @var CreateBlogCommandFactory|MockInterface */
    private $createBlogCommandFactory;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();

        $this->blogMock = Mockery::mock(Blog::class);
        $this->commandBusMock = Mockery::mock(CommandBus::class);
        $this->createCommandMock = Mockery::mock(CreateBlogCommand::class);
        $this->blogResponseBuilder = Mockery::mock(BlogResponseBuilder::class);
        $this->blogRepositoryMock = Mockery::mock(BlogRepositoryInterface::class);
        $this->createBlogCommandFactory = Mockery::mock(CreateBlogCommandFactory::class);

        $this->sut = new CreateBlogAction(
            $this->commandBusMock,
            $this->blogRepositoryMock,
            $this->blogResponseBuilder,
            $this->createBlogCommandFactory
        );
    }

    /**
     * @param array $expectedResponse
     *
     * @dataProvider responseDataProvider
     */
    public function testRun_ShouldCreateBlog(array $expectedResponse)
    {
        $address = new Url(self::BLOG_ADDRESS);

        $this->createCommandMock
            ->shouldReceive('getAddress')
            ->andReturn($address);

        $this->blogRepositoryMock
            ->shouldReceive('getByAddress')
            ->with($address)
            ->andReturn($this->blogMock);

        $this->createBlogCommandFactory
            ->shouldReceive('createFromInput')
            ->with($this->inputMock)
            ->andReturn($this->createCommandMock);

        $this->commandBusMock
            ->shouldReceive('dispatch')
            ->with($this->createCommandMock);

        $this->blogResponseBuilder
            ->shouldReceive('build')
            ->with($this->blogMock)
            ->andReturn($expectedResponse);

        $this->sut->execute($this->inputMock);
    }

    /** */
    public function testRun_ShouldThrowProperExceptionWhenBlogDoesNotExist()
    {
        $address = new Url(self::BLOG_ADDRESS);

        $this->createCommandMock
            ->shouldReceive('getAddress')
            ->andReturn($address);

        $this->blogRepositoryMock
            ->shouldReceive('getByAddress')
            ->with($address)
            ->andThrow(BlogDoesNotExistException::class);

        $this->createBlogCommandFactory
            ->shouldReceive('createFromInput')
            ->with($this->inputMock)
            ->andReturn($this->createCommandMock);

        $this->commandBusMock
            ->shouldReceive('dispatch')
            ->with($this->createCommandMock);

        $this->expectException(BadRequestException::class);

        $this->sut->execute($this->inputMock);
    }

    /**
     * @return array
     */
    public function responseDataProvider()
    {
        return [
            [
                [
                    'id' => 123,
                    'name' => 'name',
                    'address' => 'http://address.url',
                    'feed_address' => 'http://feed-address.url',
                    'created_date' => '2008-11-11 13:23:44',
                ]
            ]
        ];
    }
}
