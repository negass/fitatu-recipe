<?php

declare(strict_types=1);

namespace Recipes\CookBook\Tests\Unit\Presentation\Rest\Blog\Action;

use Mockery;
use Mockery\MockInterface;
use Recipes\CookBook\Domain\Entity\Recipe;
use Recipes\CookBook\Domain\Repository\Exception\RecipeDoesNotExistException;
use Recipes\CookBook\Domain\Repository\RecipeRepositoryInterface;
use Recipes\CookBook\Presentation\Rest\Recipe\Action\GetRecipeAction;
use Recipes\CookBook\Presentation\Rest\Recipe\ResponseBuilder\RecipeResponseBuilder;
use Recipes\SharedKernel\Infrastructure\Rest\Exception\NotFoundException;
use Recipes\SharedKernel\Infrastructure\Rest\ParameterBag;
use Recipes\SharedKernel\Test\Infrastructure\Rest\BaseActionTestCase;

/**
 * Class GetRecipeActionTest
 *
 * @covers \Recipes\CookBook\Presentation\Rest\Recipe\Action\GetRecipeAction
 */
class GetRecipeActionTest extends BaseActionTestCase
{
    const QUERY_RECIPE_ID_KEY = 'recipe_id';
    const QUERY_RECIPE_ID = '123e4567-e89b-12d3-a456-426655440000';

    /** @var GetRecipeAction */
    private $sut;

    /** @var Recipe|MockInterface */
    private $recipeMock;

    /** @var RecipeRepositoryInterface|MockInterface */
    private $recipeRepositoryMock;

    /** @var RecipeResponseBuilder|MockInterface */
    private $recipeResponseBuilderMock;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();

        $this->recipeMock = Mockery::mock(Recipe::class);
        $this->recipeRepositoryMock = Mockery::mock(RecipeRepositoryInterface::class);
        $this->recipeResponseBuilderMock = Mockery::mock(RecipeResponseBuilder::class);

        $this->sut = new GetRecipeAction(
            $this->recipeRepositoryMock,
            $this->recipeResponseBuilderMock
        );
    }

    /**
     * @param array $expectedResponse
     *
     * @dataProvider responseDataProvider
     */
    public function testRun_ShouldCreateBlog(array $expectedResponse)
    {
        $this->inputMock->parameters = new ParameterBag([
            self::QUERY_RECIPE_ID_KEY => self::QUERY_RECIPE_ID
        ]);

        $this->recipeRepositoryMock
            ->shouldReceive('get')
            ->andReturn($this->recipeMock);

        $this->recipeResponseBuilderMock
            ->shouldReceive('build')
            ->with($this->recipeMock)
            ->andReturn($expectedResponse);

        $this->assertEquals($expectedResponse, $this->sut->execute($this->inputMock));
    }

    /** */
    public function testRun_ShouldThrowProperExceptionWhenBlogDoesNotExist()
    {
        $this->inputMock->parameters = new ParameterBag([
            self::QUERY_RECIPE_ID_KEY => self::QUERY_RECIPE_ID
        ]);

        $this->recipeRepositoryMock
            ->shouldReceive('get')
            ->andThrow(RecipeDoesNotExistException::class);

        $this->expectException(NotFoundException::class);

        $this->sut->execute($this->inputMock);
    }

    /**
     * @return array
     */
    public function responseDataProvider()
    {
        return [
            [
                [
                    'id' => 123,
                    'title' => 'name',
                    'address' => 'http://address.url',
                ]
            ]
        ];
    }
}
