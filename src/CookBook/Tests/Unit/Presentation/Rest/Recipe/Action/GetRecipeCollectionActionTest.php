<?php

namespace Recipes\CookBook\Tests\Unit\Presentation\Rest\Blog\Action;

use Mockery;
use Mockery\MockInterface;
use Recipes\CookBook\Domain\Entity\Recipe;
use Recipes\CookBook\Domain\Repository\Exception\RecipeDoesNotExistException;
use Recipes\CookBook\Domain\Repository\RecipeRepositoryInterface;
use Recipes\CookBook\Presentation\Rest\Recipe\Action\GetRecipeAction;
use Recipes\CookBook\Presentation\Rest\Recipe\Action\GetRecipeCollectionAction;
use Recipes\CookBook\Presentation\Rest\Recipe\ResponseBuilder\RecipeResponseBuilder;
use Recipes\SharedKernel\Infrastructure\Rest\Exception\NotFoundException;
use Recipes\SharedKernel\Infrastructure\Rest\ParameterBag;
use Recipes\SharedKernel\Test\Infrastructure\Rest\BaseActionTestCase;

/**
 * Class GetRecipeCollectionActionTest
 *
 * @covers \Recipes\CookBook\Presentation\Rest\Recipe\Action\GetRecipeCollectionAction
 */
class GetRecipeCollectionActionTest extends BaseActionTestCase
{
    const QUERY_BLOG_ID_KEY = 'blog_id';
    const QUERY_BLOG_ID = '123e4567-e89b-12d3-a456-426655440000';

    const QUERY_LIMIT_KEY = 'limit';
    const QUERY_OFFSET_KEY = 'offset';

    const QUERY_LIMIT_VALUE = 5;
    const QUERY_OFFSET_VALUE = 2;

    /** @var GetRecipeCollectionAction */
    private $sut;

    /** @var Recipe|MockInterface */
    private $recipeMock;

    /** @var RecipeRepositoryInterface|MockInterface */
    private $recipeRepositoryMock;

    /** @var RecipeResponseBuilder|MockInterface */
    private $recipeResponseBuilderMock;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();

        $this->recipeMock = Mockery::mock(Recipe::class);
        $this->recipeRepositoryMock = Mockery::mock(RecipeRepositoryInterface::class);
        $this->recipeResponseBuilderMock = Mockery::mock(RecipeResponseBuilder::class);

        $this->sut = new GetRecipeCollectionAction(
            $this->recipeRepositoryMock,
            $this->recipeResponseBuilderMock
        );
    }

    /**
     * @param array $expectedResponse
     *
     * @dataProvider responseDataProvider
     */
    public function testRun_ShouldCreateBlog(array $expectedResponse)
    {
        $this->inputMock->parameters = new ParameterBag([
            self::QUERY_BLOG_ID_KEY => self::QUERY_BLOG_ID
        ]);

        $this->inputMock->query = new ParameterBag([
            self::QUERY_LIMIT_KEY => self::QUERY_LIMIT_VALUE,
            self::QUERY_OFFSET_KEY => self::QUERY_OFFSET_VALUE
        ]);

        $this->recipeRepositoryMock
            ->shouldReceive('getCollection')
            ->andReturn([$this->recipeMock]);

        $this->recipeResponseBuilderMock
            ->shouldReceive('buildForCollection')
            ->with([$this->recipeMock])
            ->andReturn($expectedResponse);

        $this->recipeResponseBuilderMock
            ->shouldReceive('buildForCollection')
            ->with([$this->recipeMock])
            ->andReturn($expectedResponse);

        $this->assertEquals($expectedResponse, $this->sut->execute($this->inputMock));
    }

    /** */
    public function testRun_ShouldReturnEmptyCollection()
    {
        $this->inputMock->query = new ParameterBag([
            self::QUERY_LIMIT_KEY => self::QUERY_LIMIT_VALUE,
            self::QUERY_OFFSET_KEY => self::QUERY_OFFSET_VALUE
        ]);

        $this->inputMock->parameters = new ParameterBag([
            self::QUERY_BLOG_ID_KEY => self::QUERY_BLOG_ID
        ]);

        $this->recipeRepositoryMock
            ->shouldReceive('getCollection')
            ->andReturn([]);

        $this->recipeResponseBuilderMock
            ->shouldReceive('buildForCollection')
            ->with([])
            ->andReturn([]);

        $this->assertEquals([], $this->sut->execute($this->inputMock));
    }

    /**
     * @return array
     */
    public function responseDataProvider()
    {
        return [
            [
                [
                    'id' => 123,
                    'title' => 'name',
                    'address' => 'http://address.url',
                ]
            ]
        ];
    }
}
