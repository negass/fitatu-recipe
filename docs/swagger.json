{
  "swagger": "2.0",
  "info": {
    "description": "Recipes project REST API documentation",
    "version": "0.0.1",
    "title": "Recipes project"
  },
  "host": "recipes.prod",
  "basePath": "/v1",
  "tags": [
    {
      "name": "Blogs",
    },
    {
      "name": "Recipes",
    }
  ],
  "schemes": [
    "http"
  ],
  "paths": {
    "/blogs": {
      "post": {
        "tags": [
          "Blogs"
        ],
        "summary": "Add a new blog",
        "description": "",
        "operationId": "addBlog",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Blog object that needs to be added to the store",
            "required": true,
            "schema": {
              "$ref": "#/definitions/AddBlog"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/Blog"
            }
          },
          "400": {
            "description": "",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "example": "Invalid data has been given, action is not processable",
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/blogs/{blogId}": {
      "get": {
        "tags": [
          "Blogs"
        ],
        "summary": "Get a blog",
        "description": "",
        "operationId": "getBlog",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "blogId",
            "description": "Blog id",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/Blog"
            }
          },
          "400": {
            "description": "",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "example": "Invalid data has been given, action is not processable",
                  "type": "string"
                }
              }
            }
          },
          "404": {
            "description": "",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "example": "Blog with id 418d4d13-9fda-4b0a-bc8e-8dc087918c4a does not exist",
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/blogs/{blogId}/recipes": {
      "get": {
        "tags": [
          "Recipes"
        ],
        "summary": "Get a recipes from given blog",
        "description": "",
        "operationId": "getRecipes",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "blogId",
            "description": "Blog id",
            "required": true,
            "type": "string"
          },
          {
            "in": "query",
            "name": "limit",
            "description": "Recipes limit filter",
            "required": false,
            "type": "integer",
            "default": 10
          },
          {
            "in": "query",
            "name": "offset",
            "description": "Recipes offset filter",
            "required": false,
            "type": "integer",
            "default": 0
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "type" : "array",
              "items" : {
                "$ref" : "#/definitions/Recipe"
              }
            }
          },
          "400": {
            "description": "",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "example": "Invalid data has been given, action is not processable",
                  "type": "string"
                }
              }
            }
          },
          "404": {
            "description": "",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "example": "Blog with id 418d4d13-9fda-4b0a-bc8e-8dc087918c4a does not exist",
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/recipes/{recipeId}": {
      "get": {
        "tags": [
          "Recipes"
        ],
        "summary": "Get a recipe",
        "description": "",
        "operationId": "getRecipe",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "recipeId",
            "description": "Recipe id",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/Recipe"
            }
          },
          "404": {
            "description": "",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "example": "Recipe with id 418d4d13-9fda-4b0a-bc8e-8dc087918c4a does not exist",
                  "type": "string"
                }
              }
            }
          }
        }
      }
    }
  },
  "definitions": {
    "Blog": {
      "type": "object",
      "properties": {
        "id": {
          "type": "string",
          "format": "uuid",
          "example": "2a42c5b2-9f58-4a50-85c1-a90a3c99d420"
        },
        "name": {
          "type": "string",
          "example": "Super food blog"
        },
        "address": {
          "type": "string",
          "example": "http://www.superfoodblog.com"
        },
        "feed_address": {
          "type": "string",
          "example": "http://www.superfoodblog.com/feeds/posts/default"
        },
        "created_at": {
          "$ref": "#/definitions/DateTime"
        },
        "updated_at": {
          "$ref": "#/definitions/DateTime"
        }
      },
      "xml": {
        "name": "Blog"
      }
    },
    "Recipe": {
      "type": "object",
      "properties": {
        "id": {
          "type": "string",
          "format": "uuid",
          "example": "2a42c5b2-9f58-4a50-85c1-a90a3c99d420"
        },
        "title": {
          "type": "string",
          "example": "Kotlet schabowy"
        },
        "content": {
          "type": "string",
          "example": "Super kotlet schabowy"
        },
        "feed_address": {
          "type": "string",
          "example": "http://www.superfoodblog.com/schaboszczak"
        },
        "images": {
          "type" : "array",
          "items" : {
            "$ref" : "#/definitions/RecipeImage"
          }
        },
        "created_at": {
          "$ref": "#/definitions/DateTime"
        },
        "updated_at": {
          "$ref": "#/definitions/DateTime"
        }
      },
      "xml": {
        "name": "Blog"
      }
    },
    "RecipeImage": {
      "type": "object",
      "properties": {
        "id": {
          "type": "string",
          "format": "uuid",
          "example": "2a42c5b2-9f58-4a50-85c1-a90a3c99d420"
        },
        "type": {
          "type": "string",
          "example": "original"
        },
      },
      "xml": {
        "name": "Blog"
      }
    },
    "AddBlog": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "address": {
          "type": "string"
        },
        "feed_address": {
          "type": "string"
        }
      },
      "xml": {
        "name": "AddBlog"
      }
    },
    "DateTime": {
      "type": "object",
      "properties": {
        "date": {
          "type": "string",
          "example": "2018-12-30 12:00:00"
        },
        "timezone_type": {
          "type": "integer",
          "example": 3
        },
        "timezone": {
          "type": "string",
          "example": "UTC"
        }
      },
      "xml": {
        "name": "AddBlog"
      }
    }
  }
}