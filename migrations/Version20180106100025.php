<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180106100025
 *
 * Migration that creates blog table
 */
class Version20180106100025 extends AbstractMigration
{
    /**
     * {@inheritdoc}
     */
    public function up(Schema $schema)
    {
        $this->addSql("CREATE TABLE blog (
            id CHAR(36) NOT NULL COMMENT '(DC2Type:blog_id)',
            name VARCHAR(1000) NOT NULL, 
            address VARCHAR(250) NOT NULL, 
            feedAddress VARCHAR(250) NOT NULL, 
            created_date DATETIME NOT NULL,
            UNIQUE INDEX UNIQ_ADDRESS (address), 
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB"
        );

    }

    /**
     * {@inheritdoc}
     */
    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE blog");

    }
}
