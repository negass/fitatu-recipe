<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180106101353
 *
 * Migration that creates recipe table
 */
class Version20180106101353 extends AbstractMigration
{
    /**
     * {@inheritdoc}
     */
    public function up(Schema $schema)
    {
        $this->addSql("CREATE TABLE recipe (
            id CHAR(36) NOT NULL COMMENT '(DC2Type:recipe_id)', 
            title VARCHAR(1000) NOT NULL, 
            content TEXT NOT NULL, 
            address VARCHAR(1000) NOT NULL,
            blog_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:blog_id)', 
            created_date DATETIME NOT NULL, 
            INDEX IDX_BLOG_ID (blog_id), 
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB"
        );

        $this->addSql('
            ALTER TABLE recipe 
            ADD CONSTRAINT FK_RECIPE_BLOG 
            FOREIGN KEY (blog_id) REFERENCES blog (id) 
            ON DELETE CASCADE
            ON UPDATE CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE recipe");

    }
}
